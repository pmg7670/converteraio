# The MIT License
#
# Copyright (c) 2018 Peter A McGill
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
from abc import ABCMeta, abstractmethod
from apibase import ActorGroup, ApiRequest, AppDirector, AppListener, AppResolvar, MetaReader
from apitools.hardhash import HHProvider
from apitools.csvxform import XformMetaPrvdr
from threading import RLock
import datetime
import logging
import os, sys, time
import simplejson as json
import uuid

logger = logging.getLogger('apipeer.smart')

# -------------------------------------------------------------- #
# SaasError
# ---------------------------------------------------------------#
class SaasError(Exception):
  pass

# -------------------------------------------------------------- #
# CsvToJsonSaaS
# ---------------------------------------------------------------#
class CsvToJsonSaas(AppDirector):

  def __init__(self, leveldb, actorId, caller):
    super(CsvToJsonSaas, self).__init__(leveldb, actorId)
    self.caller = caller
    self._type = 'director'
    self.state.hasNext = True
    self.resolve = Resolvar(leveldb)
    self.resolve.state = self.state
    self.request = ApiRequest()

  # -------------------------------------------------------------- #
  # _start
  # ---------------------------------------------------------------#                                          
  def _start(self, **kwargs):
    logger.info('%s._start' % self.__class__.__name__)
    mPrvdr = MetaPrvdr(self._leveldb, self.actorId)
    self.jobId, self.tsXref = mPrvdr()
    self.resolve._start(mPrvdr)
    self.mPrvdr = mPrvdr
   
  # -------------------------------------------------------------- #
  # advance
  # ---------------------------------------------------------------#                                          
  def advance(self, signal=None):
    state = self.state
    # signal = the http status code of the companion actor method
    if signal:
      state.hasSignal = False
      if signal != 201:
        errmsg = 'actor %s error, %s failed, returned error signal : %d'
        raise Exception(errmsg % (self.actorId, state.transition, signal))
      if state.transition in ('NORMALISE_ASYNC','COMPILE_ASYNC','COMPOSE_ASYNC'):
        state = self.resolveTransition(state, signal)
      elif self.state.transition == 'FINAL_HANDSHAKE':
        logger.info('FINAL_HANDSHAKE is resolved, advancing ...')
        state.inTransition = False
        state.hasNext = True
    if state.hasNext:
      state.current = state.next
    return state

  # -------------------------------------------------------------- #
  # resolveTransition
  # ---------------------------------------------------------------#                                          
  def resolveTransition(self, state, signal):
    state.signalFrom.append(signal)
    state.inTransition = len(state.signalFrom) < 2
    logger.info('### transition, %s : %s, %d' % (state.transition, str(state.inTransition), len(state.signalFrom)) )
    if state.inTransition:
      logger.info('%s is NOT yet resolved ...' % state.transition)
      return state
    logger.info('%s is resolved, advancing ...' % state.transition)
    state.hasNext = True
    state.signalFrom = []
    return state

  # -------------------------------------------------------------- #
  # quicken
  # ---------------------------------------------------------------#
  def quicken(self):
    if self.state.hasSignal:
      if self.state.transition in ['NORMALISE_ASYNC','COMPILE_ASYNC','COMPOSE_ASYNC','FINAL_HANDSHAKE']:
        self.putApiRequest(201)

  # -------------------------------------------------------------- #
  # putApiRequest
  # ---------------------------------------------------------------#
  def putApiRequest(self, signal):
    state = self.state
    logger.info('!!! putApiRequest, %s' % state.transition)
    if state.transition == 'FINAL_HANDSHAKE' or signal == 500:
      classRef = 'csvxform.csvToJsonClient:CsvToJson'
      kwargs = {"signal":signal,"callee":self.actorId,"tsXref":self.tsXref}
      pdata = (self.caller,classRef,json.dumps(kwargs))
      params = '{"type":"director","id":"%s","service":"%s","args":[],"kwargs":%s}' % pdata
      data = '{"job":%s}' % params
      apiUrl = 'http://%s/api/v1/smart' % self.mPrvdr['client:hostName']
    elif state.transition == 'NORMALISE_ASYNC':
      classRef = 'csvxform.csvToJsonXform:NormaliserFactory'
      pdata = (self.actorId,classRef,json.dumps([self.tsXref]))
      params = '{"type":"delegate","id":"%s","service":"%s","args":%s}' % pdata
      data = '{"job":%s}' % params
      apiUrl = 'http://%s/api/v1/async/%d' \
                        % (self.mPrvdr['hostName'], self.resolve.jobRange)
    elif self.state.transition == 'COMPILE_ASYNC':
      classRef = 'csvxform.csvToJsonXform:JsonCompiler'
      pdata = (self.actorId,classRef,json.dumps([self.tsXref]))
      params = '{"type":"delegate","id":"%s","service":"%s","args":%s}' % pdata
      data = '{"job":%s}' % params
      apiUrl = 'http://%s/api/v1/async/%d' % (self.mPrvdr['hostName'],1)
    elif self.state.transition == 'COMPOSE_ASYNC':
      classRef = 'csvxform.csvToJsonXform:JsonComposer'
      pdata = (self.actorId,classRef,json.dumps([self.tsXref]))
      params = '{"type":"delegate","id":"%s","service":"%s","args":%s}' % pdata
      data = '{"job":%s}' % params
      apiUrl = 'http://%s/api/v1/async/%d' % (self.mPrvdr['hostName'],1)
    else:
      return
    response = self.request.post(apiUrl,data=data)
    logger.info('%s, api response : %s' % (self.__class__.__name__,response.text))

  # -------------------------------------------------------------- #
  # onError
  # ---------------------------------------------------------------#
  def onError(self, ex):
    className = self.__class__.__name__
    if self.state.status != 'STARTED':
      errmsg = '{} actor {} error, job was {}, aborting ... ' \
        % (className, self.actorId, self.state.status)
    else:
      errmsg = '{} actor %s error, job %s was STARTED, aborting ... ' \
       % (self.actorId, self.jobId)

    logger.error(errmsg, exc_info=True)

    try:
      # notify peer about saas failure
      self.resolve.onError()
      self.putApiRequest(500)
    except Exception as ex:
      logger.error(errmsg, exc_info=True)

# -------------------------------------------------------------- #
# Resolvar
# ---------------------------------------------------------------#
class Resolvar(AppResolvar):

  def __init__(self, leveldb):
    self._leveldb = leveldb
    self.request = ApiRequest()
    self.__dict__['EVAL_XFORM_META'] = self.EVAL_XFORM_META
    self.__dict__['NORMALISE_CSV'] = self.NORMALISE_CSV
    self.__dict__['COMPILE_JSON'] = self.COMPILE_JSON
    self.__dict__['COMPOSE_JSFILE'] = self.COMPOSE_JSFILE
    self.__dict__['FINAL_HANDSHAKE'] = self.FINAL_HANDSHAKE
    self.__dict__['REMOVE_WORKSPACE'] = self.REMOVE_WORKSPACE

  # -------------------------------------------------------------- #
  # _start
  # ---------------------------------------------------------------#
  def _start(self, mPrvdr):
    self.state.current = 'EVAL_XFORM_META'
    self.jobId, self.tsXref = mPrvdr.getJobVars()
    self.mPrvdr = mPrvdr
    self._hhId = None
    msg = '%s, starting job %s ...'
    logger.info(msg % (self.__class__.__name__, self.jobId))

  # -------------------------------------------------------------- #
  # onError
  # ---------------------------------------------------------------#
  def onError(self):
    if self._hhId:
      self.closeHardHashService()

	# -------------------------------------------------------------- #
	# EVAL_XFORM_META
	# ---------------------------------------------------------------#
  def EVAL_XFORM_META(self):
    self.evalXformMeta()
    self.state.next = 'NORMALISE_CSV'
    self.state.hasNext = True
    return self.state

  # -------------------------------------------------------------- #
  # NORMALISE_CSV
  # - state.next = 'NORMAL_TO_JSON'
  # ---------------------------------------------------------------#
  def NORMALISE_CSV(self):
    self.evalSysStatus()
    self.putXformMeta()
    self.getHardHashService()
    HHProvider.joinAll(self.tsXref,'quicken')
    self.state.transition = 'NORMALISE_ASYNC'
    self.state.inTransition = True
    self.state.hasSignal = True
    self.state.next = 'COMPILE_JSON'
    self.state.hasNext = False
    return self.state

  # -------------------------------------------------------------- #
  # COMPILE_JSON
  # - state.next = 'REMOVE_WORKSPACE'
  # ---------------------------------------------------------------#
  def COMPILE_JSON(self):
    self.putJsonFileMeta()
    self.state.transition = 'COMPILE_ASYNC'
    self.state.inTransition = True
    self.state.hasSignal = True
    self.state.next = 'COMPOSE_JSFILE'
    self.state.hasNext = False
    return self.state

  # -------------------------------------------------------------- #
  # COMPOSE_JSFILE
  # - state.next = 'REMOVE_WORKSPACE'
  # ---------------------------------------------------------------#
  def COMPOSE_JSFILE(self):
    self.putJsonFileMeta()
    self.state.transition = 'COMPOSE_ASYNC'
    self.state.inTransition = True
    self.state.hasSignal = True
    self.state.next = 'FINAL_HANDSHAKE'
    self.state.hasNext = False
    return self.state

  # -------------------------------------------------------------- #
  # FINAL_HANDSHAKE
  # ---------------------------------------------------------------#
  def FINAL_HANDSHAKE(self):
    self.state.transition = 'FINAL_HANDSHAKE'
    self.state.inTransition = True
    self.state.hasSignal = True
    self.state.next = 'REMOVE_WORKSPACE'
    self.state.hasNext = False
    return self.state

  # -------------------------------------------------------------- #
  # REMOVE_WORKSPACE
  # ---------------------------------------------------------------#
  def REMOVE_WORKSPACE(self):
    self.removeWorkSpace()
    self.closeHardHashService()
    HHProvider.delete(self.tsXref)
    self.state.hasNext = False
    self.state.complete = True
    return self.state

  # -------------------------------------------------------------- #
  # evalXformMeta -
  # ---------------------------------------------------------------#
  def evalXformMeta(self):
    kwArgs = {'itemKey':'csvToJson'}
    repoMeta = self.mPrvdr.getSaasMeta('SaasXformMngr','xformDomain',kwArgs=kwArgs)
    metaFile = repoMeta['repoName'] + '/' + repoMeta['xformMeta']
    logger.info('xform meta file : ' + metaFile)
    if not os.path.exists(metaFile):
      errmsg = 'xform meta file does not exist : %s' % metaFile
      raise Exception(errmsg)

    try:
      xformMeta = XformMetaPrvdr()
      xformMeta.load(metaFile)
      xformMeta.validate('csvToJson')
    except Exception as ex:
      errmsg = '%s, xformMeta validation failed\nError : %s' % (repoMeta['xformMeta'], str(ex))
      raise Exception(errmsg)
    self.rootName = xformMeta.getRootName()
    dbKey = '%s|XFORM|rootname' % self.tsXref
    #self._leveldb.Put(dbKey,self.rootName.encode())
    self._leveldb[dbKey] = self.rootName
    self.xformMeta = xformMeta

  # -------------------------------------------------------------- #
  # evalSysStatus
  # ---------------------------------------------------------------#
  def evalSysStatus(self):
    repoMeta = self.mPrvdr.getSaasMeta('SaasRepoMngr','repoDomain')
    if not os.path.exists(repoMeta['sysPath']):
      errmsg = 'xform input path does not exist : ' + repoMeta['sysPath']
      raise Exception(errmsg)
    
    catPath = self.mPrvdr['category']
    if catPath not in repoMeta['consumer categories']:
      errmsg = 'consumer category branch %s does not exist under %s' \
                                % (catPath, str(repoMeta['consumer categories']))
      raise Exception(errmsg)
  
    repoPath = '%s/%s' % (repoMeta['sysPath'], catPath)
    logger.info('input zipfile repo path : ' + repoPath)

    inputZipFile = self.jobId + '.tar.gz'
    logger.info('input zipfile : ' + inputZipFile)

    zipFilePath = '%s/%s' % (repoPath, inputZipFile)
    if not os.path.exists(zipFilePath):
      errmsg = 'xform input zipfile does not exist in source repo'
      raise Exception(errmsg)

    if not os.path.exists(self.mPrvdr['workSpace']):
      errmsg = 'xform workspace path does not exist : ' + self.mPrvdr['workSpace']
      raise Exception(errmsg)

    workSpace = self.mPrvdr['workSpace'] + '/' + self.tsXref
    logger.info('session workspace : ' + workSpace)
    logger.info('creating session workspace ... ')

    try:
      cmdArgs = ['mkdir','-p',workSpace]      
      self.sysCmd(cmdArgs)
    except Exception as ex:
      logger.error('%s, workspace creation failed' % self.tsXref)
      raise

    try:
      cmdArgs = ['cp',zipFilePath,workSpace]
      self.sysCmd(cmdArgs)
    except Exception as ex:
      logger.error('%s, copy to workspace failed' % inputJsonFile)
      raise

    try:
      cmdArgs = ['tar','-xzf',inputZipFile]
      self.sysCmd(cmdArgs,cwd=workSpace)
    except Exception as ex:
      logger.error('%s, gunzip tar extract command failed' % inputZipFile)
      raise

    # put workspace path in storage for subprocess access
    dbKey = '%s|TASK|workspace' % self.tsXref
    #self._leveldb.Put(dbKey, workSpace.encode())
    self._leveldb[dbKey] = workSpace
    self.workSpace = workSpace

  # -------------------------------------------------------------- #
  # putXformMeta
  # ---------------------------------------------------------------#
  def putXformMeta(self):
    # put the json schema metainfo to storage for retrieval by workers
    for metaIndex, nodeName in enumerate(self.xformMeta.nodeNames):
      metaIndex += 1
      csvMeta = self.xformMeta.get(nodeName)
      dbKey = '%s|XFORM|META|%d' % (self.tsXref, metaIndex)
      #self._leveldb.Put(dbKey,json.dumps(csvMeta).encode())
      self._leveldb[dbKey] = csvMeta
      dbKey = '%s|XFORM|META|%s' % (self.tsXref, nodeName)
      #self._leveldb.Put(dbKey,json.dumps(csvMeta).encode())
      self._leveldb[dbKey] = csvMeta
      logger.info('csvToJson %s meta index : %d' % (nodeName, metaIndex))
      logger.info('%s meta item : %s ' % (nodeName, str(csvMeta)))
    self.jobRange = metaIndex

  # -------------------------------------------------------------- #
  # getHardHashService
  # ---------------------------------------------------------------#
  def getHardHashService(self):
    classRef = 'csvxform.hhCsvToJson:HHCsvToJson,HHActorFactory'
    args = [self.state.actorId,self.mPrvdr['hostName']]
    kwargs = {'default':'T1',self.rootName:'T2'}
    pdata = (classRef,json.dumps(self.xformMeta.nodeNames),json.dumps(args),json.dumps(kwargs))
    params = '{"id":null,"service":"%s","clientGroup":%s,"args":%s,"kwargs":%s}' % pdata
    data = '{"job":%s}' % params
    apiUrl = 'http://localhost:5500/api/v1/hardhash'
    response = self.request.post(apiUrl,data=data)
    logger.info('api response ' + response.text)
    rdata = json.loads(response.text) 
    if 'error' in rdata:
      raise Exception('hardhash creation failed\n' + rdata['error'])
    self._hhId = rdata['hhId'] 
    HHProvider.start(self.tsXref, rdata['routerAddr'],clients=self.xformMeta.nodeNames)
    logger.info('hardhash routerAddr : ' + rdata['routerAddr'])

  # -------------------------------------------------------------- #
  # putJsonFileMeta
  # ---------------------------------------------------------------#
  def putJsonFileMeta(self):
    jsonFile = self.jobId + '.json'
    dbKey = '%s|TASK|OUTPUT|jsonFile' % self.tsXref
    #self._leveldb.Put(dbKey, jsonFile.encode())
    self._leveldb[dbKey] = jsonFile

  # -------------------------------------------------------------- #
  # makeZipFile
  # ---------------------------------------------------------------#
  def removeWorkSpace(self):
    logger.info('removing %s workspace ...' % self.tsXref)
    cmdArgs = ['rm','-rf',self.workSpace]
    try:
      self.sysCmd(cmdArgs)
      logger.info('%s workspace is removed' % self.tsXref)      
    except Exception as ex:
      logger.error('%s, workspace removal failed' % self.tsXref)
      raise

  # -------------------------------------------------------------- #
  # closeHardHashService
  # ---------------------------------------------------------------#
  def closeHardHashService(self):
    params = '{"id":"%s"}' % self._hhId
    data = '{"job":%s}' % params
    apiUrl = 'http://localhost:5500/api/v1/hardhash'
    response = self.request.delete(apiUrl,data=data)
    logger.info('api response ' + response.text)
    rdata = json.loads(response.text)       
    if 'error' in rdata:
      errmsg = 'hardhash %s deletion failed\n%s' % (self._hhId, rdata['error'])
      logger.warn(errmsg)
    else:
      logger.info('hardhash %s is now deleted' % self._hhId)

# -------------------------------------------------------------- #
# NormaliseLstnr
# ---------------------------------------------------------------#
class NormaliseLstnr(AppListener):

  def __init__(self, leveldb, caller, callerHost):
    super().__init__(leveldb, caller)
    self.callerHost = callerHost    
    self.state = None
    self.actors = []
    self.lock = RLock()
    self.hasError = False
    self.request = ApiRequest()

  def __call__(self, event):
    with self.lock:
      if event.actorId not in self.actors or self.hasError:
        return
      if event.exception:
        self.hasError = True
        self.putApiRequest(500)
      elif self.state.transition in ['NORMALISE_ASYNC','COMPILE_ASYNC','COMPOSE_ASYNC']:
        self.evalEvent(event.actorId)
      else:
        logger.warn('got unregistered event : ' + self.state.transition)

  # -------------------------------------------------------------- #
  # evalEvent
  # ---------------------------------------------------------------#
  def evalEvent(self, actorId):
    self.actors.remove(actorId)
    if not self.actors:
      logger.info('%s, sending resume signal to caller', self.__class__.__name__)
      self.putApiRequest(201)

  # -------------------------------------------------------------- #
  # register - add a list of live job ids
  # ---------------------------------------------------------------#
  def register(self, jobRange):
    logger.info('%s register, %s' % (self.__class__.__name__, str(jobRange)))
    actorGroup = ActorGroup(self, jobRange)
    self.actors = actorGroup.ids
    return actorGroup

  # -------------------------------------------------------------- #
  # putApiRequest
  # ---------------------------------------------------------------#
  def putApiRequest(self, signal):
    classRef = 'csvxform.csvToJsonSaas:CsvToJsonSaas'
    pdata = (self.caller,classRef,json.dumps({'signal':signal}))
    params = '{"type":"director","id":"%s","service":"%s","kwargs":%s}' % pdata
    data = '{"job":%s}' % params
    apiUrl = 'http://%s/api/v1/smart' % self.callerHost
    response = self.request.post(apiUrl,data=data)
    logger.info('%s, api response : %s' % (self.__class__.__name__,response.text))

# -------------------------------------------------------------- #
# MetaPrvdr
# ---------------------------------------------------------------#
class MetaPrvdr(MetaReader):

  def __init__(self, leveldb, actorId):
    super().__init__()
    self._leveldb = leveldb
    self.actorId = actorId
    self.request = ApiRequest()
    
  # -------------------------------------------------------------- #
  # __getitem__
  # ---------------------------------------------------------------#
  def __getitem__(self, key):
    if key == 'jobId':
      return self.jobId
    elif 'client:' in key:
      key = key.split(':')[1]
      return self.jobMeta['client'][key]
    return self.jobMeta[key]

  # -------------------------------------------------------------- #
  # __setitem__
  # ---------------------------------------------------------------#
  def __setitem__(self, key, value):
    pass

  # -------------------------------------------------------------- #
  # __call__
  # ---------------------------------------------------------------#
  def __call__(self):
    pMetadoc = self.getProgramMeta()
    self.jobMeta = pMeta = pMetadoc
    logger.info('### PMETA : ' + str(pMeta))
    kwArgs = {'itemKey':pMeta['jobId']}
    _jobMeta = self.getSaasMeta('SaasEventMngr','eventDomain',queryArgs=['JOB'],kwArgs=kwArgs)
    logger.info('### JOB_META : ' + str(_jobMeta))    
    className = _jobMeta['service']
    self.jobMeta = _jobMeta[className]
    self.jobId = pMeta['jobId']
    logger.info('### SAAS JOB_META : ' + str(self.jobMeta))    
    className = _jobMeta['client'] 
    self.jobMeta['client'] = _jobMeta[className]
    logger.info('### CLIENT JOB_META : ' + str(self.jobMeta['client']))        
    self.tsXref = datetime.datetime.now().strftime('%y%m%d%H%M%S')
    logger.info('### jobId, tsXref : %s, %s' % (self.jobId, self.tsXref))
    return (self.jobId, self.tsXref)

  # -------------------------------------------------------------- #
  # getJobVars
  # -------------------------------------------------------------- #
  def getJobVars(self):
    return (self.jobId, self.tsXref)

  # -------------------------------------------------------------- #
  # getSaasMeta
  # - generic method to lookup and return xform meta
  # ---------------------------------------------------------------#
  def getSaasMeta(self, className, domainKey, hostName=None, queryArgs=[], kwArgs={}):
    classRef = 'saaspolicy.saasContract:' + className
    args = (classRef,json.dumps(queryArgs),json.dumps(kwArgs))
    params = '{"service":"%s","args":%s,"kwargs":%s}' % args
    data = '{"job":%s}' % params
    if not hostName:
      hostName = self.jobMeta['hostName']
    apiUrl = 'http://%s/api/v1/saas/%s' % (hostName, self.jobMeta[domainKey])
    response = self.request.get(apiUrl,data=data)
    result = json.loads(response.text)
    if 'error' in result:
      raise Exception(result['error'])
    return result

  # -------------------------------------------------------------- #
  # getProgramMeta
  # ---------------------------------------------------------------#
  def getProgramMeta(self):
    try:
      dbKey = 'PMETA|' + self.actorId
      #return self._leveldb.Get(dbKey)
      return self._leveldb[dbKey]
    except KeyError:
      errmsg = 'EEOWW! pmeta key error : ' + dbKey
      raise Exception(errmsg)
