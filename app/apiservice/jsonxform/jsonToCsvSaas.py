# The MIT License
#
# Copyright (c) 2018 Peter A McGill
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
from __future__ import division
from abc import ABCMeta, abstractmethod
from apibase import ActorGroup, ApiRequest, AppDirector, AppListener, AppResolvar, MetaReader
from apitools.hardhash import HHProvider
from apitools.jsonxform import XformMetaPrvdr
from threading import RLock
import datetime
import logging
import math
import os, sys, time
import simplejson as json
import uuid

logger = logging.getLogger('apipeer.smart')

# -------------------------------------------------------------- #
# JsonToCsvSaas
# ---------------------------------------------------------------#
class JsonToCsvSaas(AppDirector):

  def __init__(self, leveldb, actorId, caller):
    super(JsonToCsvSaas, self).__init__(leveldb, actorId)
    self.caller = caller
    self._type = 'director'
    self.tsXref = None
    self.state.hasNext = True
    self.resolve = Resolvar(leveldb)
    self.resolve.state = self.state
    self.request = ApiRequest()

  # -------------------------------------------------------------- #
  # _start
  # ---------------------------------------------------------------#                                          
  def _start(self, **kwargs):
    logger.info('%s._start' % self.__class__.__name__)
    mPrvdr = MetaPrvdr(self._leveldb, self.actorId)
    self.jobId, self.tsXref = mPrvdr()
    self.resolve._start(mPrvdr)
    self.mPrvdr = mPrvdr

  # -------------------------------------------------------------- #
  # advance
  # ---------------------------------------------------------------#                                          
  def advance(self, signal=None):
    state = self.state
    # signal = the http status code of the companion actor method
    if signal:
      state.hasSignal = False
      if signal != 201:
        errmsg = 'actor %s error, %s failed, returned error signal : %d'
        raise Exception(errmsg % (self.actorId, state.transition, signal))
      if state.transition in ('NORMALISE_ASYNC','COMPOSE_ASYNC'):        
        state = self.resolveTransition(state, signal)
      elif self.state.transition == 'FINAL_HANDSHAKE':
        logger.info('FINAL_HANDSHAKE is resolved, advancing ...')
        state.inTransition = False
        state.hasNext = True # handshake received now resolve RemoveWorkspace
    if state.hasNext:
      state.current = state.next
    return state

  # -------------------------------------------------------------- #
  # resolveTransition
  # ---------------------------------------------------------------#                                          
  def resolveTransition(self, state, signal):
    state.signalFrom.append(signal)
    state.inTransition = len(state.signalFrom) < 2
    logger.info('transition status, %s : %s, %d' % (state.transition, str(state.inTransition), len(state.signalFrom)) )
    if state.inTransition:
      logger.info('%s is NOT yet resolved ...' % state.transition)
      HHProvider.notifyAll(self.tsXref,'quicken')
      return state
    logger.info('%s is resolved, advancing ...' % state.transition)
    state.hasNext = True
    state.signalFrom = []
    return state

  # -------------------------------------------------------------- #
  # quicken
  # ---------------------------------------------------------------#
  def quicken(self):
    if self.state.hasSignal:
      if self.state.transition in ['NORMALISE_ASYNC','COMPOSE_ASYNC','FINAL_HANDSHAKE']:
        self.putApiRequest(201)

  # -------------------------------------------------------------- #
  # putApiRequest
  # ---------------------------------------------------------------#
  def putApiRequest(self, signal):
    state = self.state
    logger.info('!!! putApiRequest, %s' % state.transition)
    if self.state.transition == 'FINAL_HANDSHAKE' or signal == 500:
      classRef = 'jsonxform.jsonToCsvClient:JsonToCsv'
      kwargs = {"signal":signal,"callee":self.actorId,"tsXref":self.tsXref}
      pdata = (self.caller,classRef,json.dumps(kwargs))
      params = '{"type":"director","id":"%s","service":"%s","args":[],"kwargs":%s}' % pdata
      data = '{"job":%s}' % params
      apiUrl = 'http://%s/api/v1/smart' % self.mPrvdr['client:hostName']
    elif state.transition == 'NORMALISE_ASYNC':
      classRef = 'jsonxform.jsonToCsvXform:JsonNormaliser'
      pdata = (self.actorId,classRef,json.dumps([self.tsXref]))
      params = '{"type":"delegate","id":"%s","service":"%s","args":%s}' % pdata
      data = '{"job":%s}' % params
      apiUrl = 'http://%s/api/v1/async/%d' \
                        % (self.mPrvdr['hostName'], self.resolve.jobRange)
    elif self.state.transition == 'COMPOSE_ASYNC':
      classRef = 'jsonxform.jsonToCsvXform:CsvComposer'
      pdata = (self.actorId,classRef,json.dumps([self.tsXref, self.resolve.jobRange]))
      params = '{"type":"delegate","id":"%s","service":"%s","args":%s}' % pdata
      data = '{"job":%s}' % params
      apiUrl = 'http://%s/api/v1/async/%d' \
                        % (self.mPrvdr['hostName'], self.resolve.csvRange)
    else:
      logger.warn('%s transition is not implemented' % state.transition)
      return
    response = self.request.post(apiUrl,data=data)
    logger.info('api response, ' + response.text)

  # -------------------------------------------------------------- #
  # onError
  # ---------------------------------------------------------------#
  def onError(self, ex):
    if self.state.status != 'STARTED':
      errmsg = 'actor %s error, job was %s, aborting ... ' \
        % (self.actorId, self.state.status)
    else:
      errmsg = 'actor %s error, job %s was STARTED, aborting ... ' \
       % (self.actorId, self.jobId)

    logger.error(errmsg, exc_info=True)

    try:
      # notify peer about saas failure
      self.putApiRequest(500)
    except Exception as ex:
      logger.error(errmsg, exc_info=True)

# -------------------------------------------------------------- #
# getLineCount
# ---------------------------------------------------------------#
def getLineCount(fname):
  with open(fname) as f:
    for i, l in enumerate(f):
      pass
  return i

# -------------------------------------------------------------- #
# getSplitFileTag
# ---------------------------------------------------------------#
def getSplitFileTag(taskNum):
  tagOrd = int((taskNum-1) / 26)
  tagChr1 = chr(ord('a') + tagOrd)
  tagOrd = int((taskNum-1) % 26)
  tagChr2 = chr(ord('a') + tagOrd)
  return tagChr1 + tagChr2

# -------------------------------------------------------------- #
# Resolvar
# ---------------------------------------------------------------#
class Resolvar(AppResolvar):

  def __init__(self, leveldb):
    self._leveldb = leveldb
    self.request = ApiRequest()
    self.__dict__['EVAL_XFORM_META'] = self.EVAL_XFORM_META
    self.__dict__['NORMALISE_JSON'] = self.NORMALISE_JSON
    self.__dict__['COMPOSE_CSV_FILES'] = self.COMPOSE_CSV_FILES
    self.__dict__['MAKE_ZIP_FILE'] = self.MAKE_ZIP_FILE
    self.__dict__['REMOVE_WORKSPACE'] = self.REMOVE_WORKSPACE

  # -------------------------------------------------------------- #
  # _start
  # ---------------------------------------------------------------#
  def _start(self, mPrvdr):
    self.state.current = 'EVAL_XFORM_META'
    self.jobId, self.tsXref = mPrvdr.getJobVars()
    self.mPrvdr = mPrvdr
    msg = '%s, starting job %s ...'
    logger.info(msg % (self.__class__.__name__, self.jobId))

	# -------------------------------------------------------------- #
	# EVAL_CVS_META
	# ---------------------------------------------------------------#
  def EVAL_XFORM_META(self):
    self.evalXformMeta()
    state = self.state
    state.next = 'NORMALISE_JSON'
    state.hasNext = True
    return state

  # -------------------------------------------------------------- #
  # NORMALISE_JSON
  # - state.next = 'WRITE_JS_FILE'
  # ---------------------------------------------------------------#
  def NORMALISE_JSON(self):
    self.evalSysStatus()
    setSize = self.putXformMeta()
    self.getHardHashService(setSize)
    HHProvider.joinAll(self.tsXref,'quicken')
    state = self.state
    state.transition = 'NORMALISE_ASYNC'
    state.inTransition = True
    state.hasSignal = True
    state.next = 'COMPOSE_CSV_FILES'
    state.hasNext = False # depends on 2 signals, the 2nd from HH partner peer
    return state

  # -------------------------------------------------------------- #
  # COMPOSE_CSV_FILES
  # - state.next = 'MAKE_ZIP_FILE'
  # ---------------------------------------------------------------#
  def COMPOSE_CSV_FILES(self):
    self.csvRange = len(self.xformMeta.tableNames)
    HHProvider.joinAll(self.tsXref,'quicken')
    state = self.state
    state.transition = 'COMPOSE_ASYNC'
    state.inTransition = True
    state.hasSignal = True
    state.next = 'MAKE_ZIP_FILE'
    state.hasNext = False # depends on 2 signals, the 2nd from HH partner peer
    return state

  # -------------------------------------------------------------- #
  # MAKE_ZIP_FILE
  # - state.next = 'REMOVE_WORKSPACE'
  # ---------------------------------------------------------------#
  def MAKE_ZIP_FILE(self):
    self.makeGZipFile()
    state = self.state
    state.transition = 'FINAL_HANDSHAKE'
    state.inTransition = True
    state.hasSignal = True
    state.next = 'REMOVE_WORKSPACE'
    state.hasNext = False
    return state

  # -------------------------------------------------------------- #
  # REMOVE_WORKSPACE
  # ---------------------------------------------------------------#
  def REMOVE_WORKSPACE(self):
    self.removeWorkSpace()
    self.closeHardHashService()
    HHProvider.delete(self.tsXref)
    state = self.state
    state.hasNext = False
    state.complete = True
    return state

  # -------------------------------------------------------------- #
  # evalXformMeta -
  # ---------------------------------------------------------------#
  def evalXformMeta(self):
    kwArgs = {'itemKey':'jsonToCsv'}
    repoMeta = self.mPrvdr.getSaasMeta('SaasXformMngr','xformDomain',kwArgs=kwArgs)
    metaFile = repoMeta['repoName'] + '/' + repoMeta['xformMeta']
    logger.info('xform meta file : ' + metaFile)
    if not os.path.exists(metaFile):
      errmsg = 'xform meta file does not exist : %s' % metaFile
      raise Exception(errmsg)

    try:
      xformMeta = XformMetaPrvdr()
      xformMeta.load(metaFile)
      xformMeta.validate('jsonToCsv')
    except Exception as ex:
      errmsg = '%s, xformMeta validation failed\nError : %s' % (repoMeta['xformMeta'], str(ex))
      raise Exception(errmsg)
    self.rootName = xformMeta.getRootName()
    dbKey = '%s|XFORM|rootname' % self.tsXref
    #self._leveldb.Put(dbKey,self.rootName)
    self._leveldb[dbKey] = self.rootName
    self.xformMeta = xformMeta

  # -------------------------------------------------------------- #
  # evalSysStatus
  # ---------------------------------------------------------------#
  def evalSysStatus(self):
    repoMeta = self.mPrvdr.getSaasMeta('SaasRepoMngr','repoDomain')
    if not os.path.exists(repoMeta['sysPath']):
      errmsg = 'xform input path does not exist : ' + repoMeta['sysPath']
      raise Exception(errmsg)

    catPath = self.mPrvdr['category']
    if catPath not in repoMeta['consumer categories']:
      errmsg = 'consumer category branch %s does not exist under : %s ' \
                                          % (catPath, repoMeta['sysPath'])
      raise Exception(errmsg)
  
    repoPath = '%s/%s' % (repoMeta['sysPath'], catPath)
    logger.info('input json file repo path : ' + repoPath)

    inputJsonFile = '%s.%s' % (self.jobId, self.mPrvdr['fileExt'])
    logger.info('input json file : ' + inputJsonFile)

    jsonFilePath = '%s/%s' % (repoPath, inputJsonFile)
    if not os.path.exists(jsonFilePath):
      errmsg = 'xform input zip file does not exist in source repo'
      raise Exception(errmsg)

    if not os.path.exists(self.mPrvdr['workSpace']):
      errmsg = 'xform workspace path does not exist : ' + self.mPrvdr['workSpace']
      raise Exception(errmsg)

    workSpace = self.mPrvdr['workSpace'] + '/' + self.tsXref
    logger.info('session workspace : ' + workSpace)
    logger.info('creating session workspace ... ')

    try:
      cmdArgs = ['mkdir','-p',workSpace]
      self.sysCmd(cmdArgs)
    except Exception as ex:
      logger.error('%s, workspace creation failed' % self.tsXref)
      raise

    try:
      cmdArgs = ['cp',jsonFilePath,workSpace]
      self.sysCmd(cmdArgs)
    except Exception as ex:
      logger.error('%s, copy to workspace failed' % inputJsonFile)
      raise
    
    self.jsonFileName = self.jobId

    jsonFilePath = workSpace + '/' + inputJsonFile
    lineCount = getLineCount(jsonFilePath)
    self.jobRange = 2
    splitSize = int(math.ceil(lineCount / self.jobRange))
    # round up to the nearest 50
    #splitSize = int(math.ceil(splitSize / 50.0)) * 50
    try:
      cmdArgs = ['split','-l',str(splitSize),inputJsonFile,self.jsonFileName]
      self.sysCmd(cmdArgs,cwd=workSpace)
    except Exception as ex:
      logger.error('%s, split command failed' % inputJsonFile)
      raise

    for i in range(1, self.jobRange+1):
      self.putSplitFilename(i)

    dbKey = '%s|TASK|workspace' % self.tsXref
    #self._leveldb.Put(dbKey, workSpace)
    self._leveldb[dbKey] = workSpace
    self.workSpace = workSpace

  # -------------------------------------------------------------- #
  # putSplitFilename
  # ---------------------------------------------------------------#
  def putSplitFilename(self, taskNum):
    fileTag = getSplitFileTag(taskNum)
    splitFileName = self.jobId + fileTag
    dbKey = '%s|TASK|%d|INPUT|jsonFile' % (self.tsXref, taskNum)
    #self._leveldb.Put(dbKey, splitFileName.encode())
    self._leveldb[dbKey] = splitFileName

  # -------------------------------------------------------------- #
  # putXformMeta
  # ---------------------------------------------------------------#
  def putXformMeta(self):
    # tableName has a one to many relation to nodeName
    for tableIndex, tableName in enumerate(self.xformMeta.tableNames):
      tableIndex += 1
      csvPath = '%s/%s.csv' % (self.workSpace, tableName)
      dbKey = '%s|XFORM|TABLENAME|%d' % (self.tsXref, tableIndex)
      #self._leveldb.Put(dbKey, tableName.encode())
      self._leveldb[dbKey] = tableName
      dbKey = '%s|XFORM|CSVPATH|%d' % (self.tsXref, tableIndex)
      #self._leveldb.Put(dbKey, csvPath.encode())
      self._leveldb[dbKey] = csvPath
      nodeList = self.xformMeta.get(tableName=tableName)
      dbKey = '%s|XFORM|NODEMAP|%d' % (self.tsXref, tableIndex)
      #self._leveldb.Put(dbKey, json.dumps(nodeList).encode())
      self._leveldb[dbKey] = nodeList
    # put the json schema metainfo to storage for retrieval by workers
    for metaIndex, nodeName in enumerate(self.xformMeta.nodeNames):
      metaIndex += 1
      csvMeta = self.xformMeta.get(nodeName)
      dbKey = '%s|XFORM|META|%d' % (self.tsXref, metaIndex)
      #self._leveldb.Put(dbKey,json.dumps(csvMeta).encode())
      self._leveldb[dbKey] = csvMeta
      dbKey = '%s|XFORM|META|%s' % (self.tsXref, nodeName)
      #self._leveldb.Put(dbKey,json.dumps(csvMeta).encode())
      self._leveldb[dbKey] = csvMeta
      logger.info('jsonToCsv %s meta index : %d' % (nodeName, metaIndex))
      logger.info('%s meta item : %s ' % (nodeName, str(csvMeta)))
    return metaIndex

  # -------------------------------------------------------------- #
  # getHardHashService - get a HardHash service instance
  # ---------------------------------------------------------------#
  def getHardHashService(self, setSize):    
    classRef = 'jsonxform.hhJsonToCsv:HHJsonToCsv,HHActorFactory'
    args = [self.state.actorId,self.mPrvdr['hostName']]
    kwargs = {'default':'T1'}
    pdata = (classRef,json.dumps(self.xformMeta.nodeNames),json.dumps(args),json.dumps(kwargs))
    params = '{"id":null,"service":"%s","clientGroup":%s,"args":%s,"kwargs":%s}' % pdata
    data = '{"job":%s}' % params
    apiUrl = 'http://localhost:5500/api/v1/hardhash'
    response = self.request.post(apiUrl,data=data)
    logger.info('api response ' + response.text)
    rdata = json.loads(response.text) 
    if 'error' in rdata:
      raise Exception('hardhash creation failed\n' + rdata['error'])
    self.hhId = rdata['hhId'] 
    HHProvider.start(self.tsXref, rdata['routerAddr'],clients=self.xformMeta.nodeNames)
    logger.info('HardHash routerAddr : ' + rdata['routerAddr'])

  # -------------------------------------------------------------- #
  # makeGZipFile
  # ---------------------------------------------------------------#
  def makeGZipFile(self):
    gzipFile = '%s.tar.gz' % self.jobId
    logger.info('making tar gzipfile %s ...' % gzipFile)

    cmd = 'tar -czf %s *.csv' % gzipFile
    try:
      self.sysCmd(cmd,cwd=self.workSpace,shell=True)
    except Exception as ex:
      errmsg = '%s, tar gzip failed' % gzipFile
      logger.error(errmsg)
      raise

  # -------------------------------------------------------------- #
  # removeWorkSpace
  # ---------------------------------------------------------------#
  def removeWorkSpace(self):
    logger.info('removing %s workspace ...' % self.tsXref)
    cmdArgs = ['rm','-rf',self.workSpace]
    try:
      self.sysCmd(cmdArgs)
      logger.info('%s workspace is removed' % self.tsXref)      
    except Exception as ex:
      logger.error('%s, workspace removal failed' % self.tsXref)
      raise

  # -------------------------------------------------------------- #
  # closeHardHashService
  # ---------------------------------------------------------------#
  def closeHardHashService(self):
    params = '{"id":"%s"}' % self.hhId
    data = '{"job":%s}' % params
    apiUrl = 'http://localhost:5500/api/v1/hardhash'
    response = self.request.delete(apiUrl,data=data)
    logger.info('api response ' + response.text)
    rdata = json.loads(response.text)       
    if 'error' in rdata:
      errmsg = 'hardhash %s deletion failed\n%s' % (self.hhId, rdata['error'])
      logger.warn(errmsg)
    else:
      logger.info('hardhash %s is now deleted' % self.hhId)

# -------------------------------------------------------------- #
# NormaliseLstnr
# ---------------------------------------------------------------#
class NormaliseLstnr(AppListener):
  def __init__(self, leveldb, caller, callerHost):
    super(NormaliseLstnr, self).__init__(leveldb, caller)
    self.callerHost = callerHost    
    self.state = None
    self.actors = []
    self.lock = RLock()
    self.hasError = False
    self.request = ApiRequest()

  # -------------------------------------------------------------- #
  # __call__
  # ---------------------------------------------------------------#
  def __call__(self, event):
    with self.lock:
      if event.actorId not in self.actors or self.hasError:
        return
      if event.exception:
        self.hasError = True
        self.putApiRequest(500)
      elif self.state.transition in ['COMPOSE_ASYNC','NORMALISE_ASYNC']:
        self.evalEvent(event.actorId)
      else:
        logger.warn('got unregistered event : ' + self.state.transition)

  # -------------------------------------------------------------- #
  # evalEvent
  # ---------------------------------------------------------------#
  def evalEvent(self, actorId):
    self.actors.remove(actorId)
    if not self.actors:
      logger.info('%s, sending resume signal to caller', self.__class__.__name__)
      self.putApiRequest(201)

  # -------------------------------------------------------------- #
  # register - add a list of live job ids
  # ---------------------------------------------------------------#
  def register(self, jobRange):
    logger.info('%s register, %s' % (self.__class__.__name__, str(jobRange)))
    actorGroup = ActorGroup(self, jobRange)
    self.actors = actorGroup.ids
    return actorGroup

  # -------------------------------------------------------------- #
  # putApiRequest
  # ---------------------------------------------------------------#
  def putApiRequest(self, signal):
    classRef = 'jsonxform.jsonToCsvSaas:JsonToCsvSaas'
    pdata = (self.caller,classRef,json.dumps({'signal':signal}))
    params = '{"type":"director","id":"%s","service":"%s","kwargs":%s}' % pdata
    data = '{"job":%s}' % params
    apiUrl = 'http://%s/api/v1/smart' % self.callerHost
    response = self.request.post(apiUrl,data=data)
    logger.info('%s, api response : %s' % (self.__class__.__name__,response.text))

# -------------------------------------------------------------- #
# MetaPrvdr
# ---------------------------------------------------------------#
class MetaPrvdr(MetaReader):

  def __init__(self, leveldb, actorId):
    super(MetaPrvdr, self).__init__()
    self._leveldb = leveldb
    self.actorId = actorId
    self.request = ApiRequest()

  # -------------------------------------------------------------- #
  # __getitem__
  # ---------------------------------------------------------------#
  def __getitem__(self, key):
    if key == 'jobId':
      return self.jobId
    elif 'client:' in key:
      key = key.split(':')[1]
      return self.jobMeta['client'][key]
    return self.jobMeta[key]

  # -------------------------------------------------------------- #
  # __setitem__
  # ---------------------------------------------------------------#
  def __setitem__(self, key, value):
    pass

  # -------------------------------------------------------------- #
  # __call__
  # ---------------------------------------------------------------#
  def __call__(self):
    pMetadoc = self.getProgramMeta()
    self.jobMeta = pMeta = pMetadoc
    logger.info('### PMETA : ' + str(pMeta))
    kwArgs = {'itemKey':pMeta['jobId']}
    _jobMeta = self.getSaasMeta('SaasEventMngr','eventDomain',queryArgs=['JOB'],kwArgs=kwArgs)
    logger.info('### JOB_META : ' + str(_jobMeta))    
    className = _jobMeta['service']
    self.jobMeta = _jobMeta[className]
    self.jobId = pMeta['jobId']
    logger.info('### SAAS JOB_META : ' + str(self.jobMeta))    
    className = _jobMeta['client'] 
    self.jobMeta['client'] = _jobMeta[className]
    logger.info('### CLIENT JOB_META : ' + str(self.jobMeta['client']))        
    self.tsXref = datetime.datetime.now().strftime('%y%m%d%H%M%S')
    logger.info('### jobId, tsXref : %s, %s' % (self.jobId, self.tsXref))
    return (self.jobId, self.tsXref)

  # -------------------------------------------------------------- #
  # getJobVars
  # -------------------------------------------------------------- #
  def getJobVars(self):
    return (self.jobId, self.tsXref)

  # -------------------------------------------------------------- #
  # getSaasMeta
  # - generic method to lookup and return xform meta
  # ---------------------------------------------------------------#
  def getSaasMeta(self, className, domainKey, hostName=None, queryArgs=[], kwArgs={}):
    classRef = 'saaspolicy.saasContract:' + className
    args = (classRef,json.dumps(queryArgs),json.dumps(kwArgs))
    params = '{"service":"%s","args":%s,"kwargs":%s}' % args
    data = '{"job":%s}' % params
    if not hostName:
      hostName = self.jobMeta['hostName']
    apiUrl = 'http://%s/api/v1/saas/%s' % (hostName, self.jobMeta[domainKey])
    response = self.request.get(apiUrl,data=data)
    result = json.loads(response.text)
    if 'error' in result:
      raise Exception(result['error'])
    return result

  # -------------------------------------------------------------- #
  # getProgramMeta
  # ---------------------------------------------------------------#
  def getProgramMeta(self):
    try:
      dbKey = 'PMETA|' + self.actorId
      #return self._leveldb.Get(dbKey)
      return self._leveldb[dbKey]
    except KeyError:
      errmsg = 'EEOWW! pmeta key error : ' + dbKey
      raise Exception(errmsg)
