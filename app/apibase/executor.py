from abc import ABCMeta, abstractmethod
from functools import partial
from apibase import JobExecuteEvent, API_JOB_EXECUTED, API_JOB_ERROR
from threading import RLock
import asyncio
import logging
import sys
import uuid

logger = logging.getLogger('apipeer.server')
slogger = logging.getLogger('apipeer.smart')

# -------------------------------------------------------------- #
# ApiExecutor
# ---------------------------------------------------------------#
class ApiExecutor:
  def __init__(self):
    self._exec = {}
    self._exec['DIR'] = DirectorExecutor()
    self._exec['DEL'] = DelegateExecutor()

  def __getitem__(self, actorId):
    return self._exec['DIR'][actorId]

  def __setitem__(self, actorId, actor):
    self._exec['DIR'][actorId] = actor


  def runJob(self, actorId, packet):
    return self._exec['DIR'].runJob(actorId, packet)

  def runJobGroup(self, actorGroup, packet):
    return self._exec['DEL'].runJob(actorGroup, packet)

  def shutdown(self):
    self._exec['DIR'].shutdown()
    self._exec['DEL'].shutdown()

# -------------------------------------------------------------- #
# BaseExecutor
# ---------------------------------------------------------------#
class BaseExecutor:
  __metaclass__ = ABCMeta
  """
  Runs coroutines conventionally, or functions by the event loop default executor.
  """
  def __init__(self):
    self.eventloop = asyncio.get_event_loop()
    self.pendingFuture = PendingFuture()
    self.lock = RLock()

  # -------------------------------------------------------------- #
  # cancelPending
  # ---------------------------------------------------------------#
  @abstractmethod
  def cancelPending(self, *args, **kwargs):
    pass

  # -------------------------------------------------------------- #
  # getFuture
  # ---------------------------------------------------------------#
  def getFuture(self, jobUnit, packet):
     
    if isinstance(jobUnit, JobUnitAio):
      future = self.eventloop.create_task(jobUnit.job(packet))
    else:
      logMsg = 'running future: actor, args, kwargs : {}, {}, {}'
      logger.info(logMsg.format(jobUnit, packet.args, packet.kwargs))
      future = self.eventloop.run_in_executor(None, partial(jobUnit, *packet.args, **packet.kwargs))
    return future

  # -------------------------------------------------------------- #
  # shutdown
  # ---------------------------------------------------------------#
  def shutdown(self):
    self.cancelPending()

# -------------------------------------------------------------- #
# DirectorExecutor
# ---------------------------------------------------------------#
class DirectorExecutor(BaseExecutor):
  def __init__(self):
    super().__init__()
    self._actor = {}
    self.controler = JobControler()

  def __getitem__(self, actorId):
    return self._actor[actorId]

  def __setitem__(self, actorId, actor):
    self._actor[actorId] = actor

  def __delitem__(self, actorId):
    del self._actor[actorId]

  # -------------------------------------------------------------- #
  # cancelPending
  # ---------------------------------------------------------------#
  def cancelPending(self):
    [future.cancel() for future in self.pendingFuture.items if not future.done()]

  def getCompletion(self, actor):
    def complete(future):
      with self.lock:   
        actorId = actor.actorId
        actorName = actor.__class__.__name__
        try:
          retval = future.result()
        except asyncio.CancelledError:
          pass
        except Exception as ex:
          logger.error('%s actor %s errored', actorName, actorId, exc_info=True)
        else:
          logger.info('%s actor %s is complete', actorName, actorId)
        self.pendingFuture.remove(actorId, future)
        self.evalComplete(actorId, actor)
    return complete

  # -------------------------------------------------------------- #
  # getFuture
  # ---------------------------------------------------------------#
  def getFuture(self, jobUnit, packet, *args):
    future = super().getFuture(jobUnit, packet)
    callback = self.getCompletion(jobUnit)
    future.add_done_callback(callback)
    return future

  # -------------------------------------------------------------- #
  # runJob
  # ---------------------------------------------------------------#
  def runJob(self, actorId, packet):
    with self.lock:
      actor = self[actorId]
      if self.controler.hasNext(actorId, packet=packet):
        packet = self.controler.next(actorId)
        self.submit(actor, packet)     
      return {'status': 201,'actorId': actorId}

  # -------------------------------------------------------------- #
  # runNext
  # ---------------------------------------------------------------#
  def runNext(self, actor):
    actorId = actor.actorId
    if self.controler.hasNext(actorId):
      packet = self.controler.next(actorId)
      self.submit(actor, packet)

  # -------------------------------------------------------------- #
  # submit
  # ---------------------------------------------------------------#
  def submit(self, actor, packet):
    future = self.getFuture(actor, packet)
    self.pendingFuture[actor.actorId] = future

  # -------------------------------------------------------------- #
  # evalComplete
  # ---------------------------------------------------------------#
  def evalComplete(self, actorId, actor):
    try:
      actor.state
    except AttributeError:
      # only stateful jobs are retained
      del(self[actorId])
    else:
      if not actor.state.complete:
        self.runNext(actor)
        return
      logMsg = '%s director %s is complete, removing it now ...'
      if actor.state.failed:
        logMsg = '%s director %s has failed, removing it now ...'
      logger.info(logMsg, actor.__class__.__name__, actorId)
      self.removeMeta(actor)
      del(self[actorId])

  # -------------------------------------------------------------- #
  # removeMeta
  # ---------------------------------------------------------------#
  def removeMeta(self, actor):
    dbKey = 'PMETA|' + actor.actorId
    del actor.db[dbKey]

# -------------------------------------------------------------- #
# DelegateExecutor
# ---------------------------------------------------------------#
class DelegateExecutor(BaseExecutor):
  def __init__(self):
    super().__init__()
    self.pendingFuture = PendingFuture()

  # -------------------------------------------------------------- #
  # cancelPending
  # ---------------------------------------------------------------#
  def cancelPending(self):
    [self.cancelFutures(caller) for caller in self.pendingFuture.keys]

  # -------------------------------------------------------------- #
  # cancelFuture
  # ---------------------------------------------------------------#
  def cancelFutures(self, caller):
    [future.cancel() for future in self.pendingFuture[caller] if future and not future.done()]

  def getCompletion(self, actor, packet, listener):
    def complete(future):
      with self.lock:
        actorId = actor.actorId
        actorName = actor.__class__.__name__
        try:
          event = future.result()
        except asyncio.CancelledError:
          pass
        except Exception as ex:
          event = JobExecuteEvent(API_JOB_ERROR, actorId, exception=ex)
          errMsg = '%s actor %s errored, all futures will be cancelled ...'
          logger.error(errMsg, actorName, actorId, exc_info=True)
          self.cancelFutures(packet.id)
        else:
          event = JobExecuteEvent(API_JOB_EXECUTED, actorId)
          logger.info('%s actor %s is complete', actorName, actorId)
          self.pendingFuture.remove(packet.id, future)          
        if listener:
          self.eventloop.run_in_executor(None, partial(listener, event))
    return complete

  # -------------------------------------------------------------- #
  # getFuture
  # ---------------------------------------------------------------#
  def getFuture(self, jobUnit, packet, listener):
    future = super().getFuture(jobUnit, packet)
    callback = self.getCompletion(jobUnit, packet, listener)
    future.add_done_callback(callback)
    return future

  # -------------------------------------------------------------- #
  # runJob
  # ---------------------------------------------------------------#
  def runJob(self, actorGroup, packet):
    with self.lock:
      logger.info('### about to run actorGroup')
      caller = packet.id
      self.pendingFuture.clear(caller)
      packet.args.append(0)
      for jobNum, actor in actorGroup.iter('ACTOR'):
        packet.args[-1] = jobNum
        future = self.getFuture(actor, packet, listener=actorGroup.listener)
        self.pendingFuture[caller].append(future)
      return {'status': 201,'actorId': actorGroup.ids}

# -------------------------------------------------------------- #
# ActorGroup
# ---------------------------------------------------------------#
class ActorGroup:
  def __init__(self, listener, jobRange):
    self.listener = listener
    self.jobRange = jobRange
    self._actorId = {jobNum:str(uuid.uuid4()) for jobNum in jobRange}
    self._actor = {}

  def iter(self, mode):
    for jobNum in self.jobRange:
      if mode == 'ACTOR_ID':
        yield jobNum, self._actorId[jobNum]
      else:
        yield jobNum, self._actor[jobNum]

  def __setitem__(self, jobNum, data):
    self._actor[jobNum] = data

  @property
  def ids(self):
    return [id for jobNum, id in self._actorId.items()]

# -------------------------------------------------------------- #
# PendingFuture
# ---------------------------------------------------------------#
class PendingFuture:
  def __init__(self):
    self.lock = RLock()
    self._f = {}

  @property
  def keys(self):
    return self._f.keys()

  @property
  def items(self):
    return [item for key, item in self._f.items()]

  def __getitem__(self, actorId):
    with self.lock:
      try:
        return self._f[actorId]
      except KeyError:
        return None

  def __setitem__(self, actorId, future):
    with self.lock:
      self._f[actorId] = [future]

  def __delitem__(self, actorId):
    del self._f[actorId]

  def append(self, actorId, future):
    with self.lock:
      self._f[actorId].append(future)

  def clear(self, actorId):
    self._f[actorId] = []

  def remove(self, actorId, future):
    with self.lock:
      if future in self[actorId]:
        self[actorId].remove(future)
      if not self[actorId]:
        del self[actorId]

# -------------------------------------------------------------- #
# JobPacket
# ---------------------------------------------------------------#
class JobPacket:
  def __init__(self, packet):
    if not hasattr(packet,'args'):
      self.args = []
    if not hasattr(packet,'kwargs'):
      self.kwargs = {}
    if not hasattr(packet,'caller'):
      self.caller = None
    if not hasattr(packet,'responseType'):
      self.responseType = 'text'
    try:
      self.__dict__.update(packet)
    except:
      raise Exception('packet is not a dict')     

# -------------------------------------------------------------- #
# JobControler
# ---------------------------------------------------------------#
class JobControler:
  def __init__(self):
    self.cache = {}
    self.activeJob = {}
    self.lock = RLock()
    
  # -------------------------------------------------------------- #
  # hasNext
  # ---------------------------------------------------------------#
  def hasNext(self, actorId, packet=None):
    with self.lock:
      if packet: 
        # if active, append to the cache otherwise add the new job 
        if actorId in self.activeJob:
          logger.info('%s, caching job packet ...' % actorId)
          self.cache[actorId].append(packet)
          return False
        else:
          self.activeJob[actorId] = packet
          self.cache[actorId] = [packet]
          return True
      # empty packet means test if there are cached jobs ready to execute
      try:
        if len(self.cache[actorId]) > 0:
          logger.info('%s, running cached job ...' % actorId)    
          return True
      except KeyError:
        pass
      logger.info('%s, delisting ...' % actorId)
      self.activeJob.pop(actorId, None)
      self.cache.pop(actorId, None)    
      return False

  # -------------------------------------------------------------- #
  # next
  # ---------------------------------------------------------------#
  def next(self, actorId):
    with self.lock:
      logger.info('%s, running next job ...' % actorId)
      packet = self.cache[actorId].pop(0)
      self.activeJob[actorId] = packet
      return packet

# -------------------------------------------------------------- #
# JobUnit
# ---------------------------------------------------------------#
class JobUnit:
  def __init__(self, actor):
    self.actor = actor
    self.actorId = actor.actorId
    self.actorName = actor.__class__.__name__

  @staticmethod
  def get(actor):
    if asyncio.iscoroutinefunction(actor.__call__):
      return JobUnitAio(actor)
    return JobUnit(actor)

  def items(self):
    return self.actorId, self.actor

  def __call__(self, *args, **kwargs):
    try:
      self.actor(*args, **kwargs)
    except Exception as ex:
      tbInfo = (type(ex),ex,ex.__traceback__)
      tbMsg = ''.join(traceback.format_exception(tbInfo))
      event = JobExecuteEvent(API_JOB_ERROR, self.actorId, exception=ex, traceback=tbMsg)
    else:
      event = JobExecuteEvent(API_JOB_EXECUTED, self.actorId)
    return event

# -------------------------------------------------------------- #
# JobUnitAio
# ---------------------------------------------------------------#
class JobUnitAio(JobUnit):
  def __init__(self, actor):
    super().__init__(actor)

  async def __call__(self, *args, **kwargs):
    try:
      return await self.actor(*args, **kwargs)
    except Exception as ex:
      tbInfo = (type(ex),ex,ex.__traceback__)
      tbMsg = ''.join(traceback.format_exception(tbInfo))
      event = JobExecuteEvent(API_JOB_ERROR, self.actorId, exception=ex, traceback=tbMsg)
    else:
      event = JobExecuteEvent(API_JOB_EXECUTED, self.actorId)
    return event
