# The MIT License
#
# Copyright (c) 2018 Peter A McGill
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
from aiohttp import web
from apibase import ActorGroup, ApiExecutor, LeveldbHash, JobPacket
from threading import RLock
import importlib
import logging
import leveldb
import os, sys
import uuid

logger = logging.getLogger('apipeer.smart')

# ---------------------------------------------------------------------------#
# AppProvider
#
# The design intention of a smart job is to enable a group of actors to each
# run a state machine as a subprogram of an integrated super program
# The wikipedia (https://en.wikipedia.org/wiki/Actor_model) software actor 
# description says :
# In response to a message that it receives, an actor can : make local decisions, 
# create more actors, send more messages, and determine how to respond to the 
# next message received. Actors may modify their own private state, but can only 
# affect each other through messages (avoiding the need for any locks).
# ---------------------------------------------------------------------------#    
class AppProvider():

  def __init__(self):
    self.lock = RLock()

  @staticmethod
  def make(dbPath):
    logger.info('### AppProvider is starting ... ###')
    appPrvdr = AppProvider()
    appPrvdr.db = LeveldbHash(dbPath)
    appPrvdr.registry = ServiceRegister()
    appPrvdr.executor = ApiExecutor()
    return appPrvdr
    
  # -------------------------------------------------------------- #
  # addActorGroup
  # ---------------------------------------------------------------#
  def addActorGroup(self, packet, jobRange):

    callerId = packet.id
    director = self.executor[callerId]
    module, className = self.registry.getClassName(packet.service)

    actorGroup = director.listener.register(jobRange)
    for jobNum, actorId in actorGroup.iter('ACTOR_ID'):
      logger.info('### job {}, actor id : {}'.format(jobNum, actorId))
      # leveldb, actorId constructor packet are fixed by protocol
      actor = getattr(module, className)(self.db, actorId, callerId)
      # ensure delegate._type attribute is set
      actor._type = 'delegate'
      actorGroup[jobNum] = actor
    return actorGroup

  # -------------------------------------------------------------- #
  # addActor
  # ---------------------------------------------------------------#
  def addActor(self, actorId, packet):

    module, className = self.registry.getClassName(packet.service)
    # must be an AppDirector derivative, leveldb and actorId packet are fixed by protocol
    if packet.caller:
      actor = getattr(module, className)(self.db, actorId, packet.caller)
    else:
      actor = getattr(module, className)(self.db, actorId)
    if hasattr(packet, 'listener'):
      # must be an AppListener derivative, leveldb param is fixed by protocol
      module, className = self.registry.getClassName(packet.listener)
      if packet.callerHost:
        listener = getattr(module, className)(self.db, actorId, packet.callerHost)
      else:
        listener = getattr(module, className)(self.db, actorId)
      listener.state = actor.state
      actor.listener = listener
    self.executor[actorId] = actor

  # -------------------------------------------------------------- #
  # promote
  # ---------------------------------------------------------------#
  def promote(self, _packet, pmeta=None, jobRange=None):

    packet = JobPacket(_packet)
    with self.lock:
      try:
        actorId = packet.id
      except AttributeError:
        raise Exception("required param 'id' not found")
      if actorId:
        try:
          actor = self.executor[actorId]
        except KeyError:
          raise Exception('job id not found in job register : ' + actorId)
        # a live director program is promoted, ie, state machine is promoted
        try:
          logger.info('running live service actor, {}, {}'.format(actor.__class__.__name__, actorId))          
          return self.executor.runJob(actorId, packet)
        except Exception as ex:
          logger.exception('actor {} errored'.format(actorId), exc_info=True)
          return {'status': 500,'actorId': actorId,'error': str(ex)}
      else:
        actorId = str(uuid.uuid4())
        if pmeta:
          dbKey = 'PMETA|' + actorId
          self.db[dbKey] = pmeta

      # a new program, either a sync director or async delegate
      logger.info('running new service actor, {}, {}'.format(packet.service, actorId))
      try:
        self.addActor(actorId, packet)
        return self.executor.runJob(actorId, packet)
      except Exception as ex:
        logger.exception('actor {} errored'.format(actorId), exc_info=True)
        return {'status': 500,'actorId': actorId,'error': str(ex)}

  # -------------------------------------------------------------- #
  # evalJobGroup
  # ---------------------------------------------------------------#
  def evalJobGroup(self, _packet, jobRange=None):

    packet = JobPacket(_packet)
    with self.lock:
      try:
        caller = packet.id
      except AttributeError:
        raise Exception("required param 'id' not found")
      if not caller or packet.type != 'delegate':
        raise Exception('job group required packet are not valid, aborted ...')
      try:
        actor = self.executor[caller]
        logMsg = 'running actor group for live actor, {}, {}, {}'
        logger.info(logMsg.format(packet.service, actor.__class__.__name__, caller))
      except KeyError:
        raise Exception('job id not found in job register : ' + caller)
      # a live director is delegating an actor group
      if '-' in jobRange:
        a,b = list(map(int,jobRange.split('-')))
        _jobRange = range(a,b)
      else:
        b = int(jobRange) + 1
        _jobRange = range(1,b)
      actorGroup = self.addActorGroup(packet, _jobRange)
      try:
        return self.executor.runJobGroup(actorGroup, packet)
      except Exception as ex:
        logger.exception('{} actor group errored'.format(caller), exc_info=True)
        return {'status': 500,'callerId': caller,'error': str(ex)}

  # -------------------------------------------------------------- #
  # resolve
  # ---------------------------------------------------------------#
  def resolve(self, _packet):
    
    packet = JobPacket(_packet)
    module, className = self.registry.getClassName(packet.service)    
    actor = getattr(module, className)(self.db)
    with self.lock:
      try:
        if packet.kwargs:
          return actor(*packet.args, **packet.kwargs)
        return actor(*packet.args)
      except Exception as ex:
        logger.error('sync process failed : ' + str(ex))
        raise

  # -------------------------------------------------------------- #
  # resolveAio
  # ---------------------------------------------------------------#
  async def resolveAio(self, request, _packet):
    
    packet = JobPacket(_packet)
    module, className = self.registry.getClassName(packet.service)    
    actor = getattr(module, className)(self.db)
    with self.lock:
      try:
        if packet.responseType == 'stream':
          actor.response = web.StreamResponse(status=201)
          await actor.response.prepare(request)
        return await actor(*packet.args, **packet.kwargs)
      except Exception as ex:
        logger.error('sync process failed : ' + str(ex))
        raise

  # -------------------------------------------------------------- #
  # evalComplete
  # ---------------------------------------------------------------#
  temp1 = '''
  def evalComplete(self, actor, actorId):    
    with self.lock:
      try:
        actor.state
      except AttributeError:
        # only stateful jobs are retained
        del(self._job[actorId])
      else:
        if not actor.state.complete:
          return
        logMsg = '%s director %s is complete, removing it now ...'
        if actor.state.failed:
          logMsg = '%s director %s has failed, removing it now ...'
        logger.info(logMsg, actor.__class__.__name__,actorId)
        if actor._type == 'director':
          self.removeMeta(actorId)
        if hasattr(actor, 'listener'):
          self.scheduler.remove_listener(actor.listener)
        del(self._job[actorId])'''

  # -------------------------------------------------------------- #
  # removeMeta
  # ---------------------------------------------------------------#
  def removeMeta(self, actorId):
    dbKey = 'PMETA|' + actorId
    self.db[dbKey]

  # -------------------------------------------------------------- #
  # getLoadStatus
  # ---------------------------------------------------------------#
  def getLoadStatus(self, serviceName):
    with self.lock:
      if self.registry.isLoaded(serviceName):
        return {'status': 200,'loaded': True}
      return {'status': 200,'loaded': False}

  # -------------------------------------------------------------- #
  # loadService
  # ---------------------------------------------------------------#
  def loadService(self, serviceName, serviceRef):
    with self.lock:
      self.registry.loadModules(serviceName, serviceRef)

  # -------------------------------------------------------------- #
  # reloadModule
  # ---------------------------------------------------------------#
  def reloadModule(self, serviceName, moduleName):
    with self.lock:
      return self.registry.reloadModule(serviceName, moduleName)

  # -------------------------------------------------------------- #
  # shutdown
  # ---------------------------------------------------------------#
  def shutdown(self):
    with self.lock:
      self.executor.shutdown()

# -------------------------------------------------------------- #
# ServiceRegister
# ---------------------------------------------------------------#
class ServiceRegister():
	
  def __init__(self):
    self._modules = {}
    self._services = {}
    
  # ------------------------------------------------------------ #
  # isLoaded
  # -------------------------------------------------------------#
  def isLoaded(self, serviceName):
    try:
      self._services[serviceName]
    except KeyError:
      return False
    else:
      return True

  # ------------------------------------------------------------ #
  # loadModules
  # -------------------------------------------------------------#
  def loadModules(self, serviceName, serviceRef):
    self._services[serviceName] = {}
    for module in serviceRef:
      self.loadModule(serviceName, module['name'], module['fromList'])
	
  # ------------------------------------------------------------ #
  # _loadModule : wrap load module
  # -------------------------------------------------------------#
  def loadModule(self, serviceName, moduleName, fromList):
    self._services[serviceName][moduleName] = fromList
    try:
      self._modules[moduleName]
    except KeyError:
      self._loadModule(moduleName, fromList)
		
  # ------------------------------------------------------------ #
  # _loadModule : execute load module
  # -------------------------------------------------------------#
  def _loadModule(self, moduleName, fromList):    
    # reduce moduleName to the related fileName for storage
    _module = '.'.join(moduleName.split('.')[-2:])
    logger.info('%s is loaded as : %s' % (moduleName, _module))
    self._modules[_module] = __import__(moduleName, fromlist=[fromList])

  # ------------------------------------------------------------ #
  # reloadHelpModule
  # -------------------------------------------------------------#
  def reloadHelpModule(self, serviceName, helpModule):
    try:
      serviceRef = self._services[serviceName]
    except KeyError as ex:
      return ({'status':400,'errdesc':'KeyError','error':str(ex)}, 400)
    for moduleName in serviceRef:
      #_module = moduleName.split('.')[-1]
      _module = '.'.join(moduleName.split('.')[-2:])
      self._modules[_module] = None
    if helpModule not in sys.modules:
      warnmsg = '### support module %s does not exist in sys.modules'
      logger.warn(warnmsg % helpModule)
    else:
      importlib.reload(sys.modules[helpModule])
    for moduleName, fromList in serviceRef.items():
      self._loadModule(moduleName, fromList)
    logger.info('support module is reloaded : ' + helpModule)
    return {'status': 201,'service': serviceName,'module': helpModule}
	
  # ------------------------------------------------------------ #
  # reloadModule
  # -------------------------------------------------------------#
  def reloadModule(self, serviceName, moduleName):
    try:
      serviceRef = self._services[serviceName]
      fromList = serviceRef[moduleName]
    except KeyError:
      return self.reloadHelpModule(serviceName,moduleName)

    # reduce moduleName to the related fileName for storage
    #_module = moduleName.split('.')[-1]
    _module = '.'.join(moduleName.split('.')[-2:])
    self._modules[_module] = None
    importlib.reload(sys.modules[moduleName])    
    logger.info('%s is reloaded as : %s' % (moduleName, _module))
    self._modules[_module] = __import__(moduleName, fromlist=[fromList])
    return {'status': 201,'service': serviceName,'module': moduleName}
	
  # ------------------------------------------------------------ #
  # getClassName
  # -------------------------------------------------------------#
  def getClassName(self, classRef):

    if ':' not in classRef:
      raise ValueError('Invalid classRef %s, expecting module:className' % classRef)
    moduleName, className = classRef.split(':')

    try:
      module = self._modules[moduleName]
    except KeyError:
      raise Exception('Service module name not found in register : ' + moduleName)

    if not hasattr(module, className):
      raise Exception('Service classname not found in service register : ' + className)
    
    return (module, className)
