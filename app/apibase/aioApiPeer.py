#!/usr/bin/env python3
"""Example for aiohttp.web basic server
"""
from apibase import AppProvider, JobPacket, requestApprovalABC, responseApproval
from apiservice.saaspolicy import ContractError
from aiohttp import web
import asyncio
import logging
import os, sys
import simplejson as json
import textwrap

class SmartJob(web.View):

  async def post(self):
    try:
      prvdr = self.request.app['appProvider']
      rdata = self.request._rdata
      #rdata = await self.request.json()
      if 'job' not in rdata:
        return {'status':400, 'error': "required param 'job' not found"}
      jdata = rdata['job']
      pmeta = rdata['pmeta'] if 'pmeta' in rdata else None
      logger.info('job packet : ' + str(jdata))
      response = prvdr.promote(jdata, pmeta=pmeta)
    except Exception as ex:
      logger.exception('smart job failed\n%s',str(ex))
      return web.json_response({'status': 500,'error': str(ex)}, status=500)
    else:
      return web.json_response(response, status=201)

# promotes one or more asynchonous jobs
class AsyncJob(web.View):

  async def post(self):
    try:
      prvdr = self.request.app['appProvider']
      jobRange = self.request.match_info['jobRange']
      rdata = self.request._rdata
      #rdata = await self.request.json()
      if 'job' not in rdata:
        return {'status':400, 'error': "required param 'job' not found"}
      jdata = rdata['job']
      logger.info('job packet : ' + str(jdata))
      response = prvdr.evalJobGroup(jdata,jobRange=jobRange)
    except Exception as ex:
      logger.exception('async job failed\n%s',str(ex))
      return web.json_response({'status': 500,'error': str(ex)}, status=500)
    else:
      return web.json_response(response, status=201)

# adds a new program job item, and runs it (TO DO: at the datetime specified)
class SyncJob(web.View):

  async def post(self):
    try:
      prvdr = self.request.app['appProvider']
      rdata = self.request._rdata
      #rdata = await self.request.json()
      if 'job' not in rdata:
        return {'status':400, 'error': "required param 'job' not found"}
      jdata = rdata['job']
      logger.info('job packet : ' + str(jdata))
      return await prvdr.resolveAio(self.request, jdata)
    except Exception as ex:
      logger.exception('sync job failed\n%s',str(ex))
      return web.json_response({'status': 500,'error': str(ex)}, status=500)

# update a service module
class ServiceManager(web.View):

  # GET: get the service load status
  def get(self):
    prvdr = self.request.app['appProvider']
    serviceName = self.request.match_info['serviceName']      
    return prvdr.getLoadStatus(serviceName)

  # POST: reload a service module
  async def post(self):
    prvdr = self.request.app['appProvider']
    serviceName = self.request.match_info['serviceName']
    #rdata = self.request._rdata
    rdata = await self.request.json()
    if 'module' not in rdata:
      return {'status':400, 'error': "required param 'module' not found"}
    moduleName = rdata['module']
    logger.info('service, module : %s, %s', serviceName, moduleName)
    return prvdr.reloadModule(serviceName, moduleName)
    
  # PUT : load all service modules
  async def put(self):
    prvdr = self.request.app['appProvider']
    serviceName = self.request.match_info['serviceName']
    #rdata = self.request._rdata
    rdata = await self.request.json()
    if 'service' not in rdata:
      return {'status':400, 'error': "required param 'service' not found"}
    serviceRef = rdata['service']
    logger.info('service : ' + str(serviceRef))
    return prvdr.loadService(serviceName, serviceRef)

# ping to test if server is up
class Ping(web.View):

  async def get(self):
    logger.info('ping request ...')
    return web.json_response({'status': 200,'pid': os.getpid()}, status=200)

# ping to test if server is up
class SaasAdmin(web.View):

  def getUriVars(self):
    return [self.request.match_info[key] for key in ('owner', 'product', 'category')]

  async def get(self):
    try:
      prvdr = self.request.app['appProvider']
      #rdata = self.request._rdata
      rdata = await self.request.json()
      if 'job' not in rdata:
        return {'status':400, 'error': "required param 'job' not found"}
      jdata = rdata['job']
      logger.info('job packet : ' + str(jdata))

      uriVars = self.getUriVars()
      logger.info('uri vars : {} '.format(uriVars) )

      if 'args' in jdata:
        jdata['args'] = uriVars + jdata['args']
      else:    
        jdata['args'] = uriVars
      try:      
        return prvdr.resolve(jdata)
      except ContractError as ex:
        return web.json_response({'status': 400,'error': str(ex)}, status=400)
    except Exception as ex:
      return web.json_response({'status': 500,'error': str(ex)}, status=500)

  async def put(self):
    try:
      prvdr = self.request.app['appProvider']
      #rdata = self.request._rdata
      rdata = await self.request.json()
      if 'job' not in rdata:
        return {'status':400, 'error': "required param 'job' not found"}
      jdata = rdata['job']
      logger.info('job packet : ' + str(jdata))

      owner, product, category = self.getUriVars()
      label = jdata['label'].upper()

      if 'metadoc' not in rdata:
        return {'status': 400,'error': "required param 'metadoc' not found"}

      dbKey = 'SAAS|%s|%s|%s|%s' % (label, owner, product, category)
      logger.info('SAAS put key : ' + dbKey)

      prvdr.db[dbKey] = rdata['metadoc']

      msg = 'metadoc added for domain : %s' % dbKey
      return web.json_response({'status': 201,'msg': msg}, status=201)
    except Exception as ex:
      return web.json_response({'status': 500,'error': str(ex)}, status=500)

# run syscmd asynchronously
async def runCmd(cmd):
  proc = await asyncio.create_subprocess_shell(
    cmd,
    stdout=asyncio.subprocess.PIPE,
    stderr=asyncio.subprocess.PIPE)

  stdout, stderr = await proc.communicate()

  print(f'[{cmd!r} exited with {proc.returncode}]')
  if stdout:
    print(f'[stdout]\n{stdout.decode()}')
  if stderr:
    print(f'[stderr]\n{stderr.decode()}')

async def on_shutdown(app):
  prvdr = app['appProvider']
  prvdr.shutdown()
  await asyncio.sleep(1)

class ApiPeer(object):

  @staticmethod
  def addHandler(logger, logfile=None):

    if logfile:
      handler = logging.FileHandler(filename=logfile)
    else:
      handler = logging.StreamHandler(sys.stdout)
    logFormat = '%(levelname)s:%(asctime)s,%(filename)s:%(lineno)d %(message)s'
    logFormatter = logging.Formatter(logFormat, datefmt='%d-%m-%Y %I:%M:%S %p')
    handler.setFormatter(logFormatter)
    logger.addHandler(handler)

  @staticmethod    
  def _make(apiBase):

    logPath = '%s/log' % apiBase
#    if not os.path.exists(logPath):
#      await runCmd('mkdir -p {}'.format(logPath))

    logger1 = logging.getLogger('apipeer.server')
    logfile = '%s/apiServer.log' % logPath
    ApiPeer.addHandler(logger1, logfile=logfile)
    ApiPeer.addHandler(logger1)
    logger1.setLevel(logging.INFO)
    global logger
    logger = logger1

    logger2 = logging.getLogger('apipeer.smart')
    logfile = '%s/apiSmart.log' % logPath
    ApiPeer.addHandler(logger2, logfile=logfile)
    ApiPeer.addHandler(logger2)
    logger2.setLevel(logging.INFO)

    logger3 = logging.getLogger('apipeer.async')
    logfile = '%s/apiAsync.log' % logPath
    ApiPeer.addHandler(logger3, logfile=logfile)
    logger3.setLevel(logging.INFO)

    logger4 = logging.getLogger('apipeer.tools')
    logfile = '%s/apiTools.log' % logPath
    ApiPeer.addHandler(logger4, logfile=logfile)
    logger4.setLevel(logging.INFO)

    dbPath = '%s/database/metastore' % apiBase    
    return AppProvider.make(dbPath)

#    dbPath = '%s/database/metastore' % apiBase
#    if not os.path.exists(dbPath):
#      await runCmd('mkdir -p {}'.format(dbPath))
#    ApiPeer.appPrvdr = await AppProvider.make(dbPath)

def start(appPrvdr):    
  app = web.Application(middlewares=[requestApprovalABC])
  #app = web.Application()
  app.router.add_routes([
    web.post('/api/v1/smart',SmartJob),
    web.post('/api/v1/async/{jobRange}',AsyncJob),
    web.post('/api/v1/sync',SyncJob),
    web.post('/api/v1/service/{serviceName}',ServiceManager),
    web.get('/api/v1/ping',Ping),
    web.get('/api/v1/saas/{owner}/{product}/{category}',SaasAdmin),
    web.put('/api/v1/saas/{owner}/{product}/{category}',SaasAdmin)
  ])
  app['appProvider'] = appPrvdr
  app.on_shutdown.append(on_shutdown)
  return app

#async def app_factory():
#    await pre_init()
#    app = web.Application()
#    app['taskProvider'] = aioProvider()
#    app.router.add_get(...)
#    return app