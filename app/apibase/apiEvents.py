__all__ = [
  'API_EXECUTOR_ADDED', 
  'API_EXECUTOR_REMOVED',
  'API_ALL_JOBS_REMOVED',
  'API_JOB_ADDED',
  'API_JOB_REMOVED',
  'API_JOB_MODIFIED',
  'API_JOB_EXECUTED',
  'API_JOB_ERROR',
  'API_JOB_MISSED',
  'API_JOB_SUBMITTED',
  'API_JOB_MAX_INSTANCES',
  'JobEvent',
  'JobSubmitEvent',
  'JobExecuteEvent'
  ]

API_EXECUTOR_ADDED = 2 ** 0
API_EXECUTOR_REMOVED = 2 ** 1
API_ALL_JOBS_REMOVED = 2 ** 2
API_JOB_ADDED = 2 ** 3
API_JOB_REMOVED = 2 ** 4
API_JOB_MODIFIED = 2 ** 5
API_JOB_EXECUTED = 2 ** 6
API_JOB_ERROR = 2 ** 7
API_JOB_MISSED = 2 ** 8
API_JOB_SUBMITTED = 2 ** 9
API_JOB_MAX_INSTANCES = 2 ** 10

class ApiEvent:
    """
    An event that concerns the api itself.

    :ivar code: the type code of this event
    """

    def __init__(self, code):
        self.code = code

    def __repr__(self):
        return '<%s (code=%d)>' % (self.__class__.__name__, self.code)


class JobEvent(ApiEvent):
    """
    An event that concerns a job.

    :ivar code: the type code of this event
    :ivar actorId: identifier of the job in question
    """

    def __init__(self, code, actorId):
        super().__init__(code)
        self.code = code
        self.actorId = actorId

class JobSubmitEvent(JobEvent):
    """
    An event that concerns the submission of a job to its executor.

    :ivar scheduled_run_times: a list of datetimes when the job was intended to run
    """

    def __init__(self, code, actorId):
        super().__init__(code, actorId)

class JobExecuteEvent(JobEvent):
    """
    An event that concerns the running of a job within its executor.

    :ivar retval: the return value of the successfully executed job
    :ivar exception: the exception raised by the job
    :ivar traceback: a formatted traceback for the exception
    """

    def __init__(self, code, actorId, retval=None, exception=None,
                 traceback=None):
        super().__init__(code, actorId)
        self.retval = retval
        self.exception = exception
        self.traceback = traceback
