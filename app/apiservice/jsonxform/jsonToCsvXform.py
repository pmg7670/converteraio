# The MIT License
#
# Copyright (c) 2018 Peter A McGill
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
from __future__ import division
from abc import ABCMeta, abstractmethod
from apitools.hardhash import HHProvider, EmptyViewResult
from collections import deque, OrderedDict
from threading import RLock
import logging
import os, sys, time
import simplejson as json
import zmq

logger = logging.getLogger('apipeer.async')

#------------------------------------------------------------------#
# JsonNormaliser
#------------------------------------------------------------------#
class JsonNormaliser(object):

  def __init__(self, leveldb, actorId, caller):
    self._leveldb = leveldb
    self.actorId = actorId
    self.caller = caller

  # -------------------------------------------------------------- #
  # __call__
  # ---------------------------------------------------------------#
  def __call__(self, tsXref, taskNum):
    try:
      logger.info('### JsonNormaliser %d is called ... ###' % taskNum)
      dbKey = '%s|TASK|workspace' % tsXref
      #workSpace = self._leveldb.Get(dbKey)
      workSpace = self._leveldb[dbKey]
      dbKey = '%s|TASK|%d|INPUT|jsonFile' % (tsXref, taskNum)
      #self.jsonFile = self._leveldb.Get(dbKey)
      self.jsonFile = self._leveldb[dbKey]
      dbKey = '%s|XFORM|rootname' % tsXref
      #rootName = self._leveldb.Get(dbKey)
      rootName = self._leveldb[dbKey]

      logger.info('### normalise workspace : %s' % workSpace)
      logger.info('### task|%02d input json file : %s ' % (taskNum, self.jsonFile))
      logger.info('### xform root nodename : ' + rootName)

      jsonFilePath = workSpace + '/' + self.jsonFile
      if not os.path.exists(jsonFilePath):
        errmsg = '%s does not exist in workspace' % self.jsonFile
        raise Exception(errmsg)
      factory = MemberFactory(self._leveldb, tsXref, taskNum)
      self.rootMember = factory.get(rootName)
      self.normalise(jsonFilePath)
    except Exception as ex:
      logger.error('actor %s error' % self.actorId, exc_info=True)
      # apscheduler will only catch BaseException, EVENT_JOB_ERROR will not fire otherwise
      raise BaseException(ex)

	#------------------------------------------------------------------#
	# normalise
	#------------------------------------------------------------------#
  def normalise(self, jsonFilePath):
    jsonDom = list(self.getJsDomAsQueue())      
    try:
      with open(jsonFilePath) as jsfh:
        recnum = 1
        for jsRecord in jsfh:
          self.evalJsonDom(jsRecord, jsonDom)
          recnum += 1
      logger.info('### %s rowcount : %d' % (self.jsonFile, recnum))
    except Exception as ex:
      errmsg = 'splitfile, recnum : %s, %d' % (self.jsonFile, recnum)
      logger.error(errmsg)
      raise

	#------------------------------------------------------------------#
	# evalJsonDom
	#------------------------------------------------------------------#
  def evalJsonDom(self, jsRecord, jsonDom):
    jsObject = self.getJsObject(jsRecord)
    self.rootMember.evalDataset(jsObject)
    for nextMember in jsonDom:
      nextMember.evalDataset()
    self.putDatasets(jsonDom)

	#------------------------------------------------------------------#
	# getJsDomAsQueue
	#------------------------------------------------------------------#
  def getJsDomAsQueue(self):
    from collections import deque
    domQueue = deque(self.rootMember.getMembers())
    while domQueue:
      nextMember = domQueue.popleft()
      memberList = nextMember.getMembers()
      if memberList:
        domQueue.extendleft(memberList)        
      yield nextMember

	#------------------------------------------------------------------#
	# getJsObject
	#------------------------------------------------------------------#
  def getJsObject(self, jsRecord):
    try:
      return json.loads(jsRecord, object_pairs_hook=OrderedDict)
    except ValueError as ex:
      raise Exception('Json conversion failure, ' + str(ex))

	#------------------------------------------------------------------#
	# putDatasets
	#------------------------------------------------------------------#
  def putDatasets(self, jsonDom):
    hasObjects = True
    while hasObjects:
      hasObjects = self.rootMember.hasDataset
      for nextMember in jsonDom:
        hasObjects = hasObjects or nextMember.hasDataset
        if nextMember.isLeafNode:
          self.putDsByMember(nextMember)
      self.rootMember.putDataset()
    
	#------------------------------------------------------------------#
	# putDsByMember
	#------------------------------------------------------------------#
  def putDsByMember(self, nextMember):
    # child objects must be removed and put to db first before 
    # the parent object is put to db, so this means start at the
    # leaf node and traverse back up
    while not nextMember.isRoot:
      nextMember.putDataset()
      nextMember = nextMember.parent

# -------------------------------------------------------------- #
# MemberFactory
# ---------------------------------------------------------------#
class MemberFactory(object):

  def __init__(self, leveldb, tsXref, taskNum):
    self._leveldb = leveldb
    self.tsXref = tsXref
    self.taskNum = taskNum

  # -------------------------------------------------------------- #
  # get
  # ---------------------------------------------------------------#
  def get(self, nodeName, parent=None):
    dbKey = '%s|XFORM|META|%s' % (self.tsXref, nodeName)
    #metaData = self._leveldb.Get(dbKey)
    #xformMeta = json.loads(metaData)
    xformMeta = self._leveldb[dbKey]
    logger.info('jsonToCsvXform.MemberFactory - name, classTag : %s, %s ' 
        % (xformMeta['nodeName'], xformMeta['classTag']))
    try:
      className = 'JsonMember' + xformMeta['classTag']
      klass = getattr(sys.modules[__name__], className)
    except AttributeError:
      errmsg = 'xformMeta class %s does not exist in %s' % (className, __name__)
      raise Exception(errmsg)
    obj = klass(self, parent)
    obj.start(self.tsXref, self.taskNum, xformMeta)
    return obj

  # -------------------------------------------------------------- #
  # getMembers
  # ---------------------------------------------------------------#
  def getMembers(self, parent):
    memberList = []
    for nodeName in parent.children:
      logger.info('%s member : %s ' % (parent.name, nodeName))
      memberList.append(self.get(nodeName, parent))
    return memberList

#------------------------------------------------------------------#
# RowIndex
# - enables multitasking csvWriter capacity
# - the related class is a singleton and cls.lock ensures that
# - concurrent writing is atomic
#------------------------------------------------------------------#
class RowIndex(object):
  lock = None
  rowCount = None
  taskNum = None

  # -------------------------------------------------------------- #
  # incCount
  # ---------------------------------------------------------------#
  @classmethod
  def incCount(cls):
    with cls.lock:
      cls.rowCount += 1
      return '%02d|%05d' % (cls.taskNum, cls.rowCount)

	#------------------------------------------------------------------#
	# start
  # - rowcount must be a class variable, to accmulate the total count
  # - when the json DOM contains multiple node class instances
	#------------------------------------------------------------------#
  @classmethod
  def start(cls, taskNum):
    if cls.lock is None:
      logger.info('### start %s counter ###' % cls.__name__)
      cls.lock = RLock()
      cls.taskNum = taskNum
      cls.rowCount = 0

#------------------------------------------------------------------#
# JsonMember
#------------------------------------------------------------------#
class JsonMember(object):
  __metaclass__ = ABCMeta
  lock = None

  def __init__(self, factory, parent):
    self._hh = None
    self.factory = factory
    self.parent = parent
    self.isRoot = False

  # -------------------------------------------------------------- #
  # _getDataset
  # -------------------------------------------------------------- #
  @abstractmethod
  def _getDataset(self, *args, **kwargs):
    pass

	#------------------------------------------------------------------#
	# putDataset
	#------------------------------------------------------------------#
  @abstractmethod  
  def putDataset(self, *args, **kwargs):
    pass

	#------------------------------------------------------------------#
	# start
	#------------------------------------------------------------------#
  def start(self, tsXref, taskNum, xformMeta):
    self.applyMeta(tsXref, taskNum, xformMeta)
    self._hh = HHProvider.get(tsXref, self.name)

	#------------------------------------------------------------------#
	# applyMeta
	#------------------------------------------------------------------#
  def applyMeta(self, tsXref, taskNum, xformMeta):
    self.tsXref = tsXref
    self.name = xformMeta['nodeName']
    self.ukeyName = '|'.join(xformMeta['ukey']) if xformMeta['ukey'] else None
    self.fkeyName = '|'.join(xformMeta['parent']['fkey']) if not self.isRoot else None
    self.nullPolicy = xformMeta['nullPolicy']
    self.isLeafNode = xformMeta['children'] is None
    self.children = xformMeta['children']
    className = '%s%02d%s' % ('RowIndex',taskNum,xformMeta['tableTag'])
    if hasattr(sys.modules[__name__], className):
      self.rowIndex = getattr(sys.modules[__name__], className)
    else:
      rindexClass = type(className,(RowIndex,),{})
      rindexClass.start(taskNum)
      self.rowIndex = rindexClass

	#------------------------------------------------------------------#
	# getMembers
	#------------------------------------------------------------------#
  def getMembers(self):
    if self.isLeafNode:
      return None
    memberList = self.factory.getMembers(self)
    if self.isRoot:  
      return memberList
    memberList.reverse()
    return memberList

	#------------------------------------------------------------------#
	# evalDataset
  # - eval the next csvDataset, ready to put to the datastore
	#------------------------------------------------------------------#
  def evalDataset(self, csvDataset=None):
    self.csvDataset = csvDataset    
    self.hasDataset = False
    self.csvDataset = self.getDataset()
    if self.hasDataset:
      self.putHeader()

	#------------------------------------------------------------------#
	# getDataset
	#------------------------------------------------------------------#
  def getDataset(self):
    try:
      csvDataset = self._getDataset()
    except KeyError as ex:
      errmsg = '%s json object %s does not exist in csv dataset ' \
                                              % (self.name, str(ex))
      raise Exception(errmsg)
    if not csvDataset: # empty list or dict
      csvDataset = [] # if dict, convert empty dict to list
    elif not isinstance(csvDataset,list):
      csvDataset = [csvDataset]
    self.hasDataset = len(csvDataset) > 0
    return csvDataset    
    
	#------------------------------------------------------------------#
	# putHeader
	#------------------------------------------------------------------#
  def putHeader(self):
    csvObject = self.csvDataset[0]
    keyName = self.ukeyName if self.ukeyName else self.fkeyName
    keySet = set(keyName.split('|'))
    if not keySet.issubset(csvObject):
      errmsg = '%s key %s does not exist in json record' \
                                          % (self.name, keyName)
      raise Exception(errmsg)
    dbKey = '%s|header' % self.name
    self._hh[dbKey] = list(csvObject.keys())

	#------------------------------------------------------------------#
	# putCsvObject
	#------------------------------------------------------------------#
  def putCsvObject(self, csvObject):
    # self.rowIndex.incCount = klass.incCount
    rowIndex = self.rowIndex.incCount()
    dbKey = '%s|%s' % (self.name, rowIndex)
    self._hh[dbKey] = list(csvObject.values())

#------------------------------------------------------------------#
# JsonMemberRN1
#------------------------------------------------------------------#
class JsonMemberRN1(JsonMember):
  '''
  NormaliseJson, model - RootNode1
  ''' 
  def __init__(self, *args):
    super(JsonMemberRN1, self).__init__(*args)
    self.isRoot = True

  # -------------------------------------------------------------- #
  # _getDataset
  # -------------------------------------------------------------- #
  def _getDataset(self):
    return self.csvDataset.pop(self.name)

	#------------------------------------------------------------------#
	# putDataset
	#------------------------------------------------------------------#
  def putDataset(self):
    if self.hasDataset:
      csvObject = self.csvDataset.pop(0)
      self.putCsvObject(csvObject)
      self.hasDataset = len(self.csvDataset) > 0

#------------------------------------------------------------------#
# JsonMemberUKN1
#------------------------------------------------------------------#
class JsonMemberUKN1(JsonMember):
  '''
  JsonMemberJson, model - Unique Key Node 1
  ''' 
  def __init__(self, *args):
    super(JsonMemberUKN1, self).__init__(*args)

  # -------------------------------------------------------------- #
  # _getDataset
  # -------------------------------------------------------------- #
  def _getDataset(self):
    if self.parent.hasDataset:
      return self.parent.csvDataset[0].pop(self.name)

	#------------------------------------------------------------------#
	# putDataset
	#------------------------------------------------------------------#
  def putDataset(self, evalRoot=False):
    if self.hasDataset:
      csvObject = self.csvDataset.pop(0)
      self.putCsvObject(csvObject)
      self.hasDataset = len(self.csvDataset) > 0

#------------------------------------------------------------------#
# JsonMemberJsonFKN1
#------------------------------------------------------------------#
class JsonMemberFKN1(JsonMember):
  '''
  JsonMemberJson, model - Foreign Key Node 1
  ''' 
  def __init__(self, *args):
    super(JsonMemberFKN1, self).__init__(*args)

  # -------------------------------------------------------------- #
  # _getDataset
  # -------------------------------------------------------------- #
  def _getDataset(self):
    if self.parent.hasDataset:
      return self.parent.csvDataset[0][self.name]

	#------------------------------------------------------------------#
	# putDataset
	#------------------------------------------------------------------#
  def putDataset(self, evalRoot=False):
    if self.parent.hasDataset:
      csvDataset = self.parent.csvDataset[0].pop(self.name)
      for csvObject in csvDataset:
        self.putCsvObject(csvObject)
      self.hasDataset = False

#------------------------------------------------------------------#
# CsvComposer
#------------------------------------------------------------------#
class CsvComposer(object):
  def __init__(self, leveldb, actorId, caller):
    self._leveldb = leveldb
    self.actorId = actorId
    self.caller = caller

  #------------------------------------------------------------------#
	# __call__
	#------------------------------------------------------------------#
  def __call__(self, tsXref, jobRange, taskNum):
    try:
      logger.info('### CsvComposer %d is called ... ###' % taskNum)
      dbKey = '%s|XFORM|TABLENAME|%d' % (tsXref, taskNum)
      #tableName = self._leveldb.Get(dbKey)
      tableName = self._leveldb[dbKey]
      dbKey = '%s|XFORM|NODEMAP|%d' % (tsXref, taskNum)
      #nodeList = json.loads(self._leveldb.Get(dbKey))
      nodeList = self._leveldb[dbKey]
      dbKey = '%s|XFORM|CSVPATH|%d' % (tsXref, taskNum)
      #csvPath = self._leveldb.Get(dbKey)
      csvPath = self._leveldb[dbKey]

      logger.info('### task|%02d output csv file : %s.csv' % (taskNum, tableName))
      logger.info('### node list : ' + str(nodeList))

      with open(csvPath,'w') as csvfh:
        writer = CsvWriter(jobRange)
        writer.start(tsXref, nodeList[0])
        writer.writeAll(csvfh, nodeList)
        logger.info('### %s rowcount : %d' % (tableName, writer.total))
    except Exception as ex:
      logger.error('actor %s error' % self.actorId, exc_info=True)
      # apscheduler will only catch BaseException, EVENT_JOB_ERROR will not fire otherwise
      raise BaseException(ex)

#------------------------------------------------------------------#
# CsvWriter
#------------------------------------------------------------------#
class CsvWriter(object):
  def __init__(self, jobRange):
    self.jobRange = jobRange

	#------------------------------------------------------------------#
	# start
	#------------------------------------------------------------------#
  def start(self, tsXref, nodeName):
    self.nodeName = nodeName
    self._hh = HHProvider.get(tsXref, nodeName)

  #------------------------------------------------------------------#
	# writeAll
	#------------------------------------------------------------------#
  def writeAll(self, csvFh, nodeList):
    self.writeHeader(csvFh)
    self.total = 0      
    for nodeName in nodeList:
      self.write(csvFh, nodeName)

  #------------------------------------------------------------------#
	# write
	#------------------------------------------------------------------#
  def write(self, csvFh, nodeName):
    for record in self.csvDataset(nodeName):
      csvFh.write(','.join(record) + '\n')
      self.total += 1

  #------------------------------------------------------------------#
	# csvDataset
	#------------------------------------------------------------------#
  def csvDataset(self, nodeName):
    try:
      keyLow = '%s|%02d|%05d' % (nodeName,1,1)
      keyHigh = '%s|%02d|%05d' % (nodeName,self.jobRange,99999)
      return self._hh.select(keyLow,keyHigh,keysOnly=False)
    except EmptyViewResult:
      logger.warn('!!! Empty view returned')
      return []

  #------------------------------------------------------------------#
	# writeHeader
	#------------------------------------------------------------------#
  def writeHeader(self, csvfh):
    dbKey = '%s|header' % self.nodeName
    record = ','.join(self._hh[dbKey])
    csvfh.write(record + '\n')
