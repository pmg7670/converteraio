import logging
from aiohttp import web

logger = logging.getLogger('apipeer.server')

async def requestApprovalABC(app, handler):
  async def evalRequest(request):
    logger.info('requestApproval ...')
    if not request.content_type:
      return web.HTTPBadRequest(text='missing content-type')
    content_type = request.content_type.lower()
    logger.info('### content-type : ' + content_type)
    if content_type.startswith('application/json'):
      request._rdata = await request.json()
      logger.info('request json: %s' % request._rdata)
    elif content_type.startswith(('application/octet-stream','application/x-www-form-urlencoded', 'multipart/form-data')):
      #postData = await request.post()
      #request._data = dict(**postData)
      #logger.info('request form: %s' % request._data)
      request._rdata = await request.json()
      logger.info('request json: %s' % request._rdata)
    else:
      return web.HTTPBadRequest(text='Unsupported Content-Type: %s' % content_type)
    return await handler(request)
  return evalRequest

@web.middleware
async def requestApproval(request, handler):
  if not request.content_type:
    return web.HTTPBadRequest(text='missing content-type')
  logger.info('requestApproval ...')
  return await handler(request)

@web.middleware
async def responseApproval(request, handler):
  logger.info('responseFactory ...')
  try:
    response = handler(request)
  except Exception as ex:
    return web.json_response({'status': 500,'error': str(ex)}, status=500)
  if isinstance(response, web.StreamResponse):
    logger.info('### debug 1000')
    return response
  if isinstance(response, dict):
    status = response['status']
    if 'error' in response or response['mimetype'] == 'json':
      return web.json_response(response, status=status)
    elif response['mimetype'] == 'bytes':
        resp = web.Response(body=response['body'], status=status)
        resp.content_type = 'application/octet-stream'
        return resp
    elif response['mimetype'] == 'text':
      resp = web.Response(body=response['body'], status=status)
      resp.content_type = 'text/html;charset=utf-8'
      return resp
    else:
      return web.json_response(response, status=status)

async def responseApproval_ABC(app, handler):
    async def evalResponse(request):
      logger.info('responseFactory ...')
      try:
        response = handler(request)
      except Exception as ex:
        return web.json_response({'status': 500,'error': str(ex)}, status=500)
      if isinstance(response, web.StreamResponse):
        logger.info('### debug 1000')
        return response
      if isinstance(response, dict):
        status = response['status']
        if 'error' in response or response['mimetype'] == 'json':
          return web.json_response(response, status=status)
        elif response['mimetype'] == 'bytes':
            resp = web.Response(body=response['body'], status=status)
            resp.content_type = 'application/octet-stream'
            return resp
        elif response['mimetype'] == 'text':
          resp = web.Response(body=response['body'], status=status)
          resp.content_type = 'text/html;charset=utf-8'
          return resp
        else:
          return web.json_response(response, status=status)
    return evalResponse
