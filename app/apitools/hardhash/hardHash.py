from threading import RLock
import logging
import pickle
import simplejson as json
import os, sys, time
import zmq

logger = logging.getLogger('apipeer.tools')

#------------------------------------------------------------------#
# HHProvider
# factory dict id to enable multiple current sessions, where the 
# the session id is tsXref
#------------------------------------------------------------------#
class HHProvider(object):
  lock = RLock()
  factory = {}

  def __init__(self, tsXref, routerAddr):
    self.tsXref = tsXref
    self.prefix = '%s|' % tsXref
    self.routerAddr = routerAddr
    self._cache = {}
    self._context = zmq.Context.instance()
    self.lock = RLock()

	#------------------------------------------------------------------#
	# start -
  # - normaliseFactory threads depend on initial HHProvider creation
	#------------------------------------------------------------------#
  @staticmethod
  def start(tsXref, routerAddr, clients=[]):
    with HHProvider.lock:
      if tsXref not in HHProvider.factory:
        factory = HHProvider.factory[tsXref] = HHProvider(tsXref, routerAddr)
        [factory._get(clientId) for clientId in clients]

	#------------------------------------------------------------------#
	# remove
	#------------------------------------------------------------------#
  @staticmethod
  def delete(tsXref):
    with HHProvider.lock:
      if tsXref in HHProvider.factory:
        logger.info('### HHProvider %s will be deleted ...' % tsXref)
        HHProvider.factory[tsXref].terminate()
        del HHProvider.factory[tsXref]  
        logger.info('### HHProvider %s is deleted' % tsXref)

	#------------------------------------------------------------------#
	# close
	#------------------------------------------------------------------#
  @staticmethod
  def close(tsXref, clientId):
    with HHProvider.lock:
      HHProvider.factory[tsXref]._close(clientId)

	#------------------------------------------------------------------#
	# notify
	#------------------------------------------------------------------#
  @staticmethod
  def notify(tsXref, clientId, eventKey, token=None):
    with HHProvider.lock:
      hhClient = HHProvider.factory[tsXref]._get(clientId)
      hhClient.notify(eventKey,token)

	#------------------------------------------------------------------#
	# joinAll
	#------------------------------------------------------------------#
  @staticmethod
  def joinAll(tsXref, eventKey, exclude=[]):
    with HHProvider.lock:
      for hhClient in HHProvider.factory[tsXref].all(exclude):
        hhClient.join(eventKey)

	#------------------------------------------------------------------#
	# notifyAll
	#------------------------------------------------------------------#
  @staticmethod
  def notifyAll(tsXref, eventKey, exclude=[]):
    with HHProvider.lock:
      for hhClient in HHProvider.factory[tsXref].all(exclude):
        hhClient.notify(eventKey)

	#------------------------------------------------------------------#
	# get
	#------------------------------------------------------------------#
  @staticmethod
  def get(tsXref, clientId):
    with HHProvider.lock:
      return HHProvider.factory[tsXref]._get(clientId)

	#------------------------------------------------------------------#
	# _close
	#------------------------------------------------------------------#
  def _close(self, clientId):
    try:
      self._cache[clientId].close()
      del self._cache[clientId]
    except KeyError:
      raise Exception('%s factory, client %s not found in cache' % (self.tsXref, clientId))
    except zmq.ZMQError as ex:
      raise Exception('%s factory, client %s closure failed : %s' % (self.tsXref, clientId, str(ex)))

	#------------------------------------------------------------------#
	# terminate
	#------------------------------------------------------------------#
  def terminate(self):
    try:
      [client.close() for key, client in self._cache.items()]
      self._context.term()
    except zmq.ZMQError as ex:
      raise Exception('%s factory closure failed : %s' % (self.tsXref, str(ex)))

	#------------------------------------------------------------------#
	# _get
	#------------------------------------------------------------------#
  def _get(self, clientId):
    with self.lock:
      if clientId in self._cache:
        return self._cache[clientId]
      hhClient = HardHashClient.make(self._context, clientId, self.routerAddr, prefix=self.prefix)
      self._cache[clientId] = hhClient
      logger.info('### %s factory, client created by clientId : %s' % (self.tsXref, clientId))
      return hhClient

	#------------------------------------------------------------------#
	# all
	#------------------------------------------------------------------#
  def all(self, exclude=[]):
    with self.lock:
      return [hhClient for clientId, hhClient in self._cache.items() if clientId not in exclude]

#----------------------------------------------------------------#
# WrongTxnProtocol
#----------------------------------------------------------------#		
class WrongTxnProtocol(Exception):
  pass

#----------------------------------------------------------------#
# MessageTimeout
#----------------------------------------------------------------#		
class MessageTimeout(Exception):
  pass

#----------------------------------------------------------------#
# EmptyViewResult
#----------------------------------------------------------------#		
class EmptyViewResult(Exception):
  pass

#----------------------------------------------------------------#
# Messenger
#----------------------------------------------------------------#		
class Messenger():
  def __init__(self, clientId, pickleProtocol):
    self.clientId = clientId
    self._protocol = pickleProtocol
    self.sock = None

  #----------------------------------------------------------------#
  # _make
  #----------------------------------------------------------------#		
  def _make(self, context, sockType, sockAddr, hwm=1000):
    self.sock = context.socket(sockType)
    self.sock.set_hwm(hwm)
    self.sock.identity = self.clientId.encode()
    self.sock.connect(sockAddr.encode())

  #----------------------------------------------------------------#
  # putPacket
  #----------------------------------------------------------------#		
  def putPacket(self, packet, payload=None):
    try:      
      packet = [item.encode() for item in packet]      
      if payload:
        if not isinstance(payload, (bytes, bytearray)):
        # use pickle to handle any kind of object value
          payload = pickle.dumps(payload, self._protocol)
        packet.append(payload)
      self.sock.send_multipart(packet)
    except zmq.EFSM: 
      raise WrongTxnProtocol('txn protocol is out of order')
    except Exception as ex:
      logger.error('!!! putPacket failed : ' + str(ex))
      raise

  #----------------------------------------------------------------#
  # _getPacket
  #----------------------------------------------------------------#		
  def _getPacket(self):
    try:
      return self.sock.recv()
    except zmq.EFSM: 
      raise WrongTxnProtocol('txn protocol is out of order')
    except Exception as ex:
      logger.error('!!! _getPacket failed : ' + str(ex))
      raise

#----------------------------------------------------------------#
# HardHashClient
#----------------------------------------------------------------#		
class HardHashClient(Messenger):
  def __init__(self, clientId, prefix, pickleProtocol=pickle.HIGHEST_PROTOCOL):
    super().__init__(clientId, pickleProtocol)
    self._prefix = prefix
    self._debug = False
    self.restoreMode = 'PYOBJ'
    self.lock = RLock()

  #----------------------------------------------------------------#
  # make
  #----------------------------------------------------------------#		
  @staticmethod
  def make(context, clientId, routerAddr, prefix=None, **kwargs):
    logger.info('### creating HardHash client, routerAddr : ' + routerAddr)
    client = HardHashClient(clientId, prefix)
    client._make(context, zmq.DEALER, routerAddr, **kwargs)
    return client

  #----------------------------------------------------------------#
  # setRestoreMode
  #----------------------------------------------------------------#		
  def setRestoreMode(self, mode):
    self.restoreMode = mode

  #----------------------------------------------------------------#
  # close
  #----------------------------------------------------------------#		
  def close(self):
    with self.lock:
      try:
        if not self.sock.closed:
          self.sock.setsockopt(zmq.LINGER, 0)          
          self.sock.close()
          logger.info('### client socket is closed')
      except zmq.ZMQError as ex:
        logger.error('### client closure failed : ' + str(ex))

  #----------------------------------------------------------------#
  # notify
  #----------------------------------------------------------------#		
  def notify(self, eventKeys, token=None):
    with self.lock:
      try:
        token = token if token else self.clientId
        logger.info('!!! NOTIFY : %s, %s' % (eventKeys, token))
        self.putPacket(['NOTIFY',eventKeys,token,'201'])
      except Exception as ex:
        logger.error('notify failed : ' + str(ex))
        raise

  #----------------------------------------------------------------#
  # join
  #----------------------------------------------------------------#		
  def join(self, eventKeys, token=None):
    with self.lock:
      try:
        token = token if token else self.clientId
        logger.info('!!! JOIN : %s, %s' % (eventKeys, token))
        self.putPacket(['JOIN',eventKeys,token])
      except Exception as ex:
        logger.error('notify failed : ' + str(ex))
        raise

  #----------------------------------------------------------------#
  # MutableMapping methods for emulating a dict
  #----------------------------------------------------------------#		

  #----------------------------------------------------------------#
  # __getitem__
  #----------------------------------------------------------------#		
  def __getitem__(self, key):
    with self.lock:
      try:
        key = '%s%s' % (self._prefix, key) if self._prefix else key
        self.putPacket(['GET',key])
        return self.getPacket('GET')
      except KeyError as ex:
        logger.warn('GET key error : ' + key)
        return None
      except Exception as ex:
        logger.error('GET failed : ' + str(ex))
        return None

  #----------------------------------------------------------------#
  # __setitem__
  #----------------------------------------------------------------#		
  def __setitem__(self, key, value):
    with self.lock:    
      try:
        key = '%s%s' % (self._prefix, key) if self._prefix else key
        self.putPacket(['PUT',key],payload=value)
      except Exception as ex:
        logger.error('PUT failed : ' + str(ex))
        raise

  #----------------------------------------------------------------#
  # __delitem__
  #----------------------------------------------------------------#		
  def __delitem__(self, key):
    with self.lock:    
      try:
        key = '%s%s' % (self._prefix, key) if self._prefix else key      
        self.putPacket(['DELETE',key])
      except Exception as ex:
        logger.error('DELETE failed : ' + str(ex))
        raise

  #----------------------------------------------------------------#
  # getPacket
  #----------------------------------------------------------------#		
  def getPacket(self, action):
    value = pickle.loads(self._getPacket())
    if value == 'KEY_ERROR':
      raise KeyError()
    if value == 'HH_ERROR':
      raise Exception('%s failed' % action)
    try:
      if self.restoreMode == 'JSON':
        return json.dumps(value)
      return value
    except ValueError:
      return value

  #----------------------------------------------------------------#
  # getGenerator
  #----------------------------------------------------------------#		
  def getGenerator(self):
    firstValue = self.getPacket('SELECT')
    if not firstValue:
      raise EmptyViewResult()
    yield firstValue
    while self.sock.getsockopt(zmq.RCVMORE):
      yield self.getPacket('SELECT')

  #----------------------------------------------------------------#
  # append
  #----------------------------------------------------------------#		
  def append(self, key, value):
    with self.lock:
      try:
        key = '%s%s' % (self._prefix, key) if self._prefix else key      
        self.putPacket(['APPEND',key],payload=value)
      except Exception as ex:
        logger.error('APPEND failed : ' + str(ex))
        raise

  #----------------------------------------------------------------#
  # select
  # - use with caution - because the related socket is bound to the
  # - output generator, the socket is unusable until generator is complete
  #----------------------------------------------------------------#		
  def select(self, keyLow, keyHigh, keysOnly=False):
    with self.lock:
      try:
        keyLow = self._prefix + keyLow if self._prefix else keyLow
        keyHigh = self._prefix + keyHigh if self._prefix else keyHigh      
        logger.info('### SELECT keyLow, keyHigh : %s, %s' % (keyLow, keyHigh))
        self.putPacket(['SELECT',keyLow,keyHigh,str(keysOnly)])
        return self.getGenerator()
      except Exception as ex:
        logger.error('SELECT failed : ' + str(ex))
        raise

  #----------------------------------------------------------------#
  # minmax
  # get the resolved bounds of a range
  #----------------------------------------------------------------#		
  def minmax(self, keyLow, keyHigh):
    with self.lock:
      try:
        keyLow = self._prefix + keyLow if self._prefix else keyLow
        keyHigh = self._prefix + keyHigh if self._prefix else keyHigh      
        logger.info('### MINMAX keyLow, keyHigh : %s, %s' % (keyLow, keyHigh))
        self.putPacket(['MINMAX',keyLow,keyHigh])
        return self.getPacket('MINMAX')
      except Exception as ex:
        logger.error('MINMAX failed : ' + str(ex))
        raise

  #----------------------------------------------------------------#
  # handleError
  #----------------------------------------------------------------#		
  def handleError(self, action):
    logger.error('!!! %s FAILED' % action)
    raise Exception('%s failed' % action)
    
  #----------------------------------------------------------------#
  # redundant methods
  #----------------------------------------------------------------#		

  def __iter__(self):
    raise Exception('Not implemented')

  def __len__(self):
    raise Exception('Not implemented')

  def __enter__(self):
    return self

  def __exit__(self, type, value, traceback):
    pass

  def __contains__(self, key):
    raise Exception('Not implemented')
