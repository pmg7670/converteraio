# The MIT License
#
# Copyright (c) 2018 Peter A McGill
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
from __future__ import division
from abc import ABCMeta, abstractmethod
from apibase import ApiRequest, AppDirector, AppState, AppResolvar, AppListener, MetaReader
from collections import deque, OrderedDict
from threading import RLock
import logging
import simplejson as json
import os, sys, time

logger = logging.getLogger('apipeer.smart')

# -------------------------------------------------------------- #
# CsvToJson
# ---------------------------------------------------------------#
class CsvToJson(AppDirector):

  def __init__(self, leveldb, actorId):
    super(CsvToJson, self).__init__(leveldb, actorId)
    self._type = 'director'
    self.state.hasNext = True
    self.resolve = Resolvar()
    self.resolve.state = self.state
    self.request = ApiRequest()

  # -------------------------------------------------------------- #
  # runApp
  # - override to get the callee actorId sent from CsvToJsonSaas
  # ---------------------------------------------------------------#
  def runApp(self, signal=None, callee=None, tsXref=None):
    if callee:
      msg = '%s.runApp - callee, tsXref : %s, %s'
      logger.info(msg % (self.__class__.__name__, callee, tsXref))
      self.callee = callee
      self.resolve.tsXref = tsXref
    super(CsvToJson, self).runApp(signal)

  # -------------------------------------------------------------- #
  # _start
  # ---------------------------------------------------------------#                                          
  def _start(self, **kwargs):
    logger.info('%s._start' % self.__class__.__name__)
    mPrvdr = MetaPrvdr(self._leveldb, self.actorId)
    self.jobId = mPrvdr()
    self.resolve._start(mPrvdr)
    self.mPrvdr = mPrvdr
    
  # -------------------------------------------------------------- #
  # advance
  # -------------------------------------------------------------- #
  def advance(self, signal=None):
    state = self.state
    # signal = the http status code of the companion actor method
    if signal:
      state.hasSignal = False
      if signal != 201:
        errmsg = 'actor %s error, %s failed, returned error signal : %d'
        raise Exception(errmsg % (self.actorId, state.transition, signal))
      if state.transition == 'CSV_TO_JSON_SAAS':
        logger.info('CSV_TO_JSON_SAAS is resolved, advancing ...')
        state.inTransition = False
        state.hasNext = True
    if state.hasNext:
      state.current = state.next
    return state
    
  # -------------------------------------------------------------- #
  # quicken
  # ---------------------------------------------------------------#
  def quicken(self):
    if self.state.hasSignal:
      self.putApiRequest()
    
  # -------------------------------------------------------------- #
  # putApiRequest
  # ---------------------------------------------------------------#
  def putApiRequest(self):
    state = self.state
    if state.complete:
      logger.info('handshake response to callee : ' + self.callee)
      classRef = 'csvxform.csvToJsonSaas:CsvToJsonSaas'
      pdata = (self.callee,classRef,json.dumps({"signal":201}))
      params = '{"type":"director","id":"%s","service":"%s","args":[],"kwargs":%s}' % pdata
      data = '{"job":%s}' % params
      apiUrl = 'http://%s/api/v1/smart' % self.mPrvdr['service:hostName']
    elif state.transition == 'CSV_TO_JSON_SAAS':
      classRef = 'csvxform.csvToJsonSaas:CsvToJsonSaas'
      listener = 'csvxform.csvToJsonSaas:NormaliseLstnr'
      pdata = (classRef,listener,self.actorId,self.mPrvdr['service:hostName'])
      params = '{"type":"director","id":null,"service":"%s","listener":"%s","caller":"%s","callerHost":"%s"}' % pdata
      pmeta = self.mPrvdr.getProgramMeta()
      data = '{"job":%s,"pmeta":%s}' % (params, json.dumps(pmeta))
      apiUrl = 'http://%s/api/v1/smart' % self.mPrvdr['service:hostName']
    else:
      logger.warn('%s transition is not implemented as expected' % state.transition)
      return
    response = self.request.post(apiUrl, data=data)
    logger.info('%s, api response : %s' % (self.__class__.__name__,response.text))

  # -------------------------------------------------------------- #
  # onError
  # ---------------------------------------------------------------#
  def onError(self, ex):
    if self.state.status != 'STARTED':
      errmsg = 'actor %s error, job was %s, aborting ... ' \
        % (self.actorId, self.state.status)
    else:
      errmsg = 'actor %s error, job %s was STARTED, aborting ... ' \
       % (self.actorId, self.jobId)

    logger.error(errmsg, exc_info=True)

# -------------------------------------------------------------- #
# Resolvar
# ---------------------------------------------------------------#
class Resolvar(AppResolvar):
  
  def __init__(self):
    self.request = ApiRequest()
    self.__dict__['CSV_TO_JSON'] = self.CSV_TO_JSON
    self.__dict__['DOWNLOAD_ZIPFILE'] = self.DOWNLOAD_ZIPFILE

  # -------------------------------------------------------------- #
  # _start
  # ---------------------------------------------------------------#
  def _start(self, mPrvdr):
    self.state.current = 'CSV_TO_JSON'
    self.jobId = mPrvdr.getJobVars()
    self.mPrvdr = mPrvdr
    msg = '%s.Resolvar, starting job %s ...'
    logger.info(msg % (self.__class__.__name__,self.jobId))

  # -------------------------------------------------------------- #
  # CSV_TO_JSON
  # - state.next = 'WRITE_JSON_FILE'
  # ---------------------------------------------------------------#
  def CSV_TO_JSON(self):
    state = self.state
    state.transition = 'CSV_TO_JSON_SAAS'
    state.inTransition = True
    state.hasSignal = True
    state.next = 'DOWNLOAD_ZIPFILE'
    state.hasNext = False
    return state

  # -------------------------------------------------------------- #
  # DOWNLOAD_ZIPFILE
  # ---------------------------------------------------------------#
  def DOWNLOAD_ZIPFILE(self):
    self.downloadJsonFile()
    self.uncompressFile()
    state = self.state
    state.transition = 'NA'
    state.hasSignal = True
    state.hasNext = False
    state.complete = True
    return state

  # -------------------------------------------------------------- #
  # downloadJsonFile
  # ---------------------------------------------------------------#
  def downloadJsonFile(self):
    repoMeta = self.mPrvdr.getSaasMeta('SaasRepoMngr','repoDomain')
    if not os.path.exists(repoMeta['sysPath']):
      errmsg = 'xform output path does not exist : ' + repoMeta['sysPath']
      raise Exception(errmsg)

    catPath = self.mPrvdr['category']
    if catPath not in repoMeta['consumer categories']:
      errmsg = 'consumer category branch %s does not exist under : %s ' \
                                          % (catPath, repoMeta['sysPath'])
      raise Exception(errmsg)
  
    self.repoPath = '%s/%s' % (repoMeta['sysPath'], catPath)
    logger.info('output json gzipfile repo path : ' + self.repoPath)

    self.jsonZipFile = '%s.%s' % (self.jobId, self.mPrvdr['fileExt'])
    logger.info('output json gzipfile : ' + self.jsonZipFile)

    zipFilePath = '%s/%s' % (self.repoPath, self.jsonZipFile)
    dstream = self.getFileStream()

    try:
      with open(zipFilePath, 'wb') as fhwb:
        for chunk in dstream.iter_content(chunk_size=1024): 
          if chunk: # filter out keep-alive new chunks
            fhwb.write(chunk)
    except Exception as ex:
      errmsg = '%s write failed : ' + self.jsonZipFile
      logger.error(errmsg)
      raise

  # -------------------------------------------------------------- #
  # getFileStream
  # ---------------------------------------------------------------#
  def getFileStream(self):
    try:
      dstream = self._getFileStream()
    except Exception as ex:
      errmsg = 'json gzip file stream api request failed'
      logger.error(errmsg)
      raise
    if dstream.status_code != 201:
      errmsg = 'json gzip file stream api request failed\nError : %s' % dstream.text
      raise Exception(errmsg)
    return dstream

  # -------------------------------------------------------------- #
  # _getFileStream
  # ---------------------------------------------------------------#
  def _getFileStream(self):
    data = (self.tsXref, self.jsonZipFile)
    params = '{"service":"datatxn.dataTxnPrvdr:BinryFileStreamPrvdr","responseType":"stream","args":%s}' \
                                                        % json.dumps(data)
    data = '{"job":%s}' % params
    apiUrl = 'http://%s/api/v1/sync' % self.mPrvdr['service:hostName']
    # NOTE: stream=True parameter
    return self.request.post(apiUrl,data=data,stream=True)

  # -------------------------------------------------------------- #
  # uncompressFile
  # ---------------------------------------------------------------#
  def uncompressFile(self):
    logger.info('%s, gunzip ...' % self.jsonZipFile)
    try:
      cmdArgs = ['gunzip','-f',self.jsonZipFile]
      self.sysCmd(cmdArgs,cwd=self.repoPath)
    except Exception as ex:
      errmsg = '%s, gunzip failed' % self.jsonZipFile
      logger.error(errmsg)
      raise

# -------------------------------------------------------------- #
# MetaPrvdr
# ---------------------------------------------------------------#
class MetaPrvdr(MetaReader):

  def __init__(self, leveldb, actorId):
    super(MetaPrvdr, self).__init__()
    self._leveldb = leveldb
    self.actorId = actorId
    self.request = ApiRequest()
    self.pMeta = {}

  # -------------------------------------------------------------- #
  # __getitem__
  # ---------------------------------------------------------------#
  def __getitem__(self, key):
    if key == 'jobId':
      return self.jobId
    elif 'service:' in key:
      key = key.split(':')[1]
      return self.jobMeta['service'][key]
    return self.jobMeta[key]

  # -------------------------------------------------------------- #
  # __setitem__
  # ---------------------------------------------------------------#
  def __setitem__(self, key, value):
    pass

  # -------------------------------------------------------------- #
  # __call__
  # ---------------------------------------------------------------#
  def __call__(self):
    pMetadoc = self.getProgramMeta()
    self.jobMeta = pMeta = pMetadoc
    logger.info('### PMETA : ' + str(pMeta))
    kwArgs = {'itemKey':pMeta['jobId']}
    _jobMeta = self.getSaasMeta('SaasEventMngr','eventDomain',queryArgs=['JOB'],kwArgs=kwArgs)
    logger.info('### JOB_META : ' + str(_jobMeta))    
    className = _jobMeta['client']
    self.jobMeta = _jobMeta[className]
    self.jobId = pMeta['jobId']
    logger.info('### CLIENT JOB_META : ' + str(self.jobMeta))    
    className = _jobMeta['service'] 
    self.jobMeta['service'] = _jobMeta[className]
    logger.info('### SAAS JOB_META : ' + str(self.jobMeta['service']))
    logger.info('### jobId : %s' % self.jobId)
    return self.jobId

  # -------------------------------------------------------------- #
  # getJobVars
  # -------------------------------------------------------------- #
  def getJobVars(self):
    return self.jobId

  # -------------------------------------------------------------- #
  # getSaasMeta
  # - generic method to lookup and return xform meta
  # ---------------------------------------------------------------#
  def getSaasMeta(self, className, domainKey, hostName=None, queryArgs=[], kwArgs={}):
    classRef = 'saaspolicy.saasContract:' + className
    args = (classRef,json.dumps(queryArgs),json.dumps(kwArgs))
    params = '{"service":"%s","args":%s,"kwargs":%s}' % args
    data = '{"job":%s}' % params
    if not hostName:
      hostName = self.jobMeta['hostName']
    apiUrl = 'http://%s/api/v1/saas/%s' % (hostName, self.jobMeta[domainKey])
    response = self.request.get(apiUrl,data=data)
    result = json.loads(response.text)
    if 'error' in result:
      raise Exception(result['error'])
    return result

  # -------------------------------------------------------------- #
  # getProgramMeta
  # ---------------------------------------------------------------#
  def getProgramMeta(self):
    try:
      dbKey = 'PMETA|' + self.actorId
      #return self._leveldb.Get(dbKey)
      return self._leveldb[dbKey]
    except KeyError:
      errmsg = 'EEOWW! pmeta key error : ' + dbKey
      raise Exception(errmsg)
